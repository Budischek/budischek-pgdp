package Blatt3;
import MiniJava.MiniJava;
/**
 * Created by david on 09.11.16.
 * 17 + 4, kartenwert 2-11, Spieler nacheinander, über 21 verliert sofort
 * Inputs: 1 = Karte ziehen, 0 = ende;
 * Höhere Zahl gewinnt, bei unentschieden gewinnt Spieler 1
 */
public class Kartenspiel extends MiniJava{
    public static void main(String[] args) {
        int startingHand = 2;
        String exitMsg = "The game will now exit.";
        write("Player 1's turn. Starting with drawing your starting hand (" + startingHand + " cards).");
        int player1 = calculateDraw(0, startingHand);
        if (player1 == 21) {
            write("Player 1 scored 21, thus winning. " + exitMsg);
        }
        else if (player1 == 99) {
            write("Incorrect Input detected. " + exitMsg); //could also call calculateDraw with player1 as value to continue the game after an incorrect input
        }
        else if (player1 > 21) {
            write("Player 1 drew more than 21, Player 2 wins the game. " + exitMsg);
        }
        else {
            write("Player 2's turn. Starting with drawing your starting hand (" + startingHand + " cards).");
            int player2 = calculateDraw(0, startingHand);
            if (player2 == 99) {
                write("Incorrect Input detected. " + exitMsg); //could also call calculateDraw with player2 as value to continue the game after an incorrect input
            }
            else if(player2 > 21) {
                write("Player 2 drew more than 21, Player 1 wins the game. " + exitMsg);
            }
            else if(player1>player2) {
                write("Player 1 drew closer to 21 than Player 2, Player 1 wins the game. " + exitMsg);
            }
            else if(player2>player1) {
                write("Player 2 drew closer to 21 than Player 1, Player 2 wins the game. " + exitMsg);
            }
            else if(player1==player2) {
                write("Player 1 and Player 2 drew the same value, Player 1 wins the game. " + exitMsg);
            }
        }
        System.exit(1);
    }

    /**
     * Recursively draw cards until either:
     *      the accumulated card value is over 21
     *      the starting hand is drawn (no more cardsToDraw
     * @param currentValue accumulated card value
     * @param cardsToDraw amount of cards to draw until user input is required (starting hand)
     * @return accumulated card value at the end of recursion
     */
    public static int calculateDraw(int currentValue, int cardsToDraw) {
        int value = currentValue;
        int card = drawCard();
        write("You drew: " + card);
        value += card;
        if(value > 20) {
            return value;
        }
        else if(--cardsToDraw > 0) {
            return calculateDraw(value, cardsToDraw-1);
        }
        else {
            return calculateDraw(value);
        }
    }
    /**
     * Recursively draw cards until either:
     *      the player choses to not draw another card
     *      the accumulated card value is over 21
     *      an incorrect input is detected
     * @param currentValue accumulated card value
     * @return accumulated card value at the end of recursion
     */
    public static int calculateDraw(int currentValue) {
        int value = currentValue;
        int input = readInt("Your current value is " + value + ". Do you want to draw another card? (1 = yes, 0 = no)");

        if(input==0) {
            return currentValue;
        }

        if(input==1) {
            int card = drawCard();
            write("You drew: " + card);
            value += card;
            if(value > 20) {
                return value;
            }
            else {
                return calculateDraw(value);
            }
        }

        return 99;
    }
}
