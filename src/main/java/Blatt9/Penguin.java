package Blatt9;

import java.util.Arrays;

public class Penguin extends Predator {

    // Ein Pinguin kann 12 Tage bzw. Spielrunden ohne Essen auskommen.
    // Die Deklaration darf entfernt (und der Wert z. B. direkt im Code
    // verwendet) werden.
    private static int withoutFood = 12;


    /**
     * Dem Konstruktor wird das Geschlecht des Tiers uebergeben.
     *
     */
    public Penguin(boolean female) {
        super(female);
        this.daysLeft = withoutFood;
    }

    public Penguin(boolean female, Position position, String square) {
        super(female, position, square);
        this.daysLeft = withoutFood;
    }

    @Override
    public Move[] possibleMoves() {
        char column = 'a';
        int row = 0;
        try {
            column = square.charAt(0);
            row = square.charAt(1) - '0';
        }
        catch (IndexOutOfBoundsException e) {
            return new Move[0];
        }

        Move[] moves = new Move[8];
        int nrMoves = 0;

        for(int cOffset = -1; cOffset < 2; cOffset++) {
            for(int rOffset = -1; rOffset < 2; rOffset++) {
                if(isViableSquare(row + rOffset, (char) (column + cOffset)) >= 0) {
                    nrMoves ++;
                    moves[nrMoves - 1] = new Move(square, "" + (char) (column + cOffset) + (row + rOffset));
                }

            }
        }

        moves = Arrays.copyOfRange(moves, 0, nrMoves);
        return moves;
    }

    @Override
    public void consume() {
        daysLeft = withoutFood;
    }

    @Override
    public String toString(){
        return this.female
          ? (Globals.darkSquare(this.square) ? Globals.ts_female_penguin_dark : Globals.ts_female_penguin_light)
          : (Globals.darkSquare(this.square) ? Globals.ts_male_penguin_dark : Globals.ts_male_penguin_light);
    }
}
