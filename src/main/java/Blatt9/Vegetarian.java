package Blatt9;
/**
 * Klasse der Vegetarier.
 */
public class Vegetarian extends Animal {

    /**
     * Dem Konstruktor wird das Geschlecht des Tiers uebergeben.
     *
     */
    public Vegetarian(boolean female) {
        super(female);
    }

    public Vegetarian(boolean female, Position position, String square) {
        super(female, position, square);
    }


    @Override
    protected int isViableSquare(int row, char col) {
        // '`' is the char you get when you decrease 'a' by 1
        if('`' < col && col < 'i' && row >= 1 && row <= 8) {
            Animal a = position.findAnimal("" + col + row);
            if(a == null) {
                return 1;
            }
        }
        return -1;
    }

    @Override
    public void sunset() {
        //do nothing
    }
}
