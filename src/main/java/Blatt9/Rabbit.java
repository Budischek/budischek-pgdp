package Blatt9;

import java.util.Arrays;

public class Rabbit extends Vegetarian {

    /**
     * Dem Konstruktor wird das Geschlecht des Tiers uebergeben.
     *
     */
    public Rabbit(boolean female) {
        super(female);
    }

    public Rabbit(boolean female, Position position, String square) {
        super(female, position, square);
    }

    @Override
    public Move[] possibleMoves() {
        char column = 'a';
        int row = 0;
        try {
            column = square.charAt(0);
            row = square.charAt(1) - '0';
        }
        catch (IndexOutOfBoundsException e) {
            return new Move[0];
        }

        Move[] moves = new Move[8];
        int nrMoves = 0;
        char tmpC;
        int tmpR;

        for(int cOffset = -1; cOffset < 2; cOffset++) {
            for(int rOffset = -1; rOffset < 2; rOffset++) {
                tmpC = (char) (column + cOffset);
                tmpR = row + rOffset;
                if(isViableSquare(tmpR, tmpC) >= 0) {
                    nrMoves ++;
                    moves[nrMoves - 1] = new Move(square, "" + tmpC + tmpR);
                }
            }
        }

        moves = Arrays.copyOfRange(moves, 0, nrMoves);
        return moves;
    }

    @Override
    public String toString(){
        return this.female
          ? (Globals.darkSquare(this.square) ? Globals.ts_female_rabbit_dark : Globals.ts_female_rabbit_light)
          : (Globals.darkSquare(this.square) ? Globals.ts_male_rabbit_dark : Globals.ts_male_rabbit_light);
    }

}
