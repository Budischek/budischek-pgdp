package Blatt9;
/**
 * Die Klasse Move repraesentiert einen einzelnen Zug.
 *
 * Es gibt zwei Konstruktoren. Einer bekommt
 * Ausgangsfeld und Zielfeld uebergeben, der andere
 * bekommt nur den eingegebenen Zug in der Form
 * <Ausgangsfeld><Zielfeld> als String uebergeben,
 * also z. B. "a7c5" fuer den Zug von "a7" nach "c5".
 */
public class Move {
    String from;
    String to;

    public Move(String from, String to){
        if(Position.isValidSquare(from) && Position.isValidSquare(to)) {
            this.from = from;
            this.to = to;
        }
        else {
            this.to = "XX";
            this.from = "XX";
        }
    }

    public Move(String move){
        if(move.length() == 4) {
            from = "" + move.charAt(0) + move.charAt(1);
            to = "" + move.charAt(2) + move.charAt(3);
        }
        else {
            to = "XX";
            from = "XX";
        }
    }

    @Override
    public String toString(){
        return from + to;
    }

    public boolean equals(Object other) {
        if(other instanceof Move && to.equals(((Move) other).to) && from.equals(((Move) other).from)) return true;
        return false;
    }

}
