package Blatt9;

import java.util.Arrays;

public class Horse extends Vegetarian {

    /**
     * Dem Konstruktor wird das Geschlecht des Tiers uebergeben.
     *
     */
    public Horse(boolean female) {
        super(female);
    }

    public Horse(boolean female, Position position, String square) {
        super(female, position, square);
    }

    @Override
    public Move[] possibleMoves() {
        char column = 'a';
        int row = 0;
        try {
            column = square.charAt(0);
            row = square.charAt(1) - '0';
        }
        catch (IndexOutOfBoundsException e) {
            return new Move[0];
        }

        Move[] moves = new Move[12];
        int nrMoves = 0;

        int rowOffset = 1, columnOffset = 0, count = 0;

        while(count < 12) {
            if (isViableSquare(row + rowOffset, (char) (column + columnOffset)) >= 0) {
                nrMoves++;
                moves[nrMoves - 1] = new Move(square, "" + ((char) (column + columnOffset)) + (row + rowOffset));
            }

            count++;
            switch (count) {
                case 1:
                    rowOffset = -1;
                    break;
                case 2:
                    rowOffset = 0;
                    columnOffset = 1;
                    break;
                case 3:
                    columnOffset = -1;
                    break;
                case 4:
                    columnOffset = 2;
                    rowOffset = 2;
                    break;
                case 5:
                    rowOffset = -2;
                    break;
                case 6:
                    columnOffset = -2;
                    break;
                case 7:
                    columnOffset = -2;
                    rowOffset = 2;
                    break;
                case 8:
                    columnOffset = 3;
                    rowOffset = 3;
                    break;
                case 9:
                    rowOffset = -3;
                    break;
                case 10:
                    columnOffset = -3;
                    break;
                case 11:
                    columnOffset = -3;
                    rowOffset = 3;
                    break;
            }
        }

        moves = Arrays.copyOfRange(moves, 0, nrMoves);
        return moves;
    }

    @Override
    public String toString(){
        return this.female
          ? (Globals.darkSquare(this.square) ? Globals.ts_female_horse_dark : Globals.ts_female_horse_light)
          : (Globals.darkSquare(this.square) ? Globals.ts_male_horse_dark : Globals.ts_male_horse_light);
    }

}
