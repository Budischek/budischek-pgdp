package Blatt9;

import java.util.Arrays;

public class Leopard extends Predator {

    // Ein Leopard kann nur 5 Tage bzw. Spielrunden ohne Essen auskommen.
    // Die Deklaration darf entfernt (und der Wert z. B. direkt im Code
    // verwendet) werden.
    private static int withoutFood = 5;


    /**
     * Dem Konstruktor wird das Geschlecht des Tiers uebergeben.
     *
     */
    public Leopard(boolean female) {
        super(female);
        this.daysLeft = withoutFood;
    }

    public Leopard(boolean female, Position position, String square) {
        super(female, position, square);
        this.daysLeft = withoutFood;
    }

    @Override
    public Move[] possibleMoves() {
        char column = 'a';
        int row = 0;
        try {
            column = square.charAt(0);
            row = square.charAt(1) - '0';
        }
        catch (IndexOutOfBoundsException e) {
            return new Move[0];
        }

        Move[] moves = new Move[30];
        int nrMoves = 0;

        nrMoves = findHorizontalMoves(moves, nrMoves);
        nrMoves = findVerticalMoves(moves, nrMoves);
        nrMoves = findDiagonalMoves(moves, nrMoves);

        moves = Arrays.copyOfRange(moves, 0, nrMoves);
        return moves;
    }

    private int findDiagonalMoves(Move[] moves, int nrMoves) {
        char column = square.charAt(0);
        int row = square.charAt(1) - '0';
        int viable, rowOffset = 1, columnOffset = 1, count = 0;

        while(count < 4) {
            for (int offset = 1; offset < 8; offset++) {
                viable = isViableSquare(row + offset * rowOffset, (char) (column + offset * columnOffset));
                if (viable >= 0) {
                    nrMoves++;
                    moves[nrMoves - 1] = new Move(square, "" + ((char) (column + offset * columnOffset)) + (row + offset * rowOffset));
                }
                if (viable <= 0) {
                    break;
                }

            }

            count++;
            switch (count) {
                case 1:
                    rowOffset = -1;
                    columnOffset = 1;
                    break;
                case 2:
                    rowOffset = -1;
                    columnOffset = -1;
                    break;
                case 3:
                    rowOffset = 1;
                    columnOffset = -1;
                    break;
            }
        }
        return nrMoves;
    }

    private int findVerticalMoves(Move[] moves, int nrMoves) {
        char column = square.charAt(0);
        int row = square.charAt(1) - '0';
        int viable;

        //Moves towards the bottom
        for(int rOffset = -1; rOffset > -8; rOffset--) {
            viable = isViableSquare(row + rOffset, column);
            if(viable >= 0) {
                nrMoves++;
                moves[nrMoves - 1] = new Move(square, "" + column + (row + rOffset));
            }
            if(viable <= 0) {
                rOffset = -9;
            }
        }

        //Moves towards the top
        for(int rOffset = 1; rOffset < 8; rOffset++) {
            viable = isViableSquare(row + rOffset, column);
            if(viable >= 0) {
                nrMoves++;
                moves[nrMoves - 1] = new Move(square, "" + column + (row + rOffset));
            }
            if(viable <= 0) {
                rOffset = 9;
            }
        }

        return nrMoves;
    }

    private int findHorizontalMoves(Move[] moves, int nrMoves) {
        char column = square.charAt(0);
        int row = square.charAt(1) - '0';
        int viable;
        //Moves towards the left
        for(int cOffset = -1; cOffset > -8; cOffset--) {
            viable = isViableSquare(row, (char) (column + cOffset));
            if(viable >= 0) {
                nrMoves++;
                moves[nrMoves - 1] = new Move(square, "" + ((char) (column + cOffset)) + row);
            }
            if(viable <= 0) {
                cOffset = -9;
            }
        }

        //Moves towards the right
        for(int cOffset = 1; cOffset < 8; cOffset++) {
            viable = isViableSquare(row, (char) (column + cOffset));
            if(viable >= 0) {
                nrMoves++;
                moves[nrMoves - 1] = new Move(square, "" + ((char) (column + cOffset)) + row);
            }
            if(viable <= 0) {
                cOffset = 9;
            }
        }

        return nrMoves;
    }

    @Override
    public void consume() {
        daysLeft = withoutFood;
    }

    @Override
    public String toString(){
        return this.female
          ? (Globals.darkSquare(this.square) ? Globals.ts_female_leopard_dark : Globals.ts_female_leopard_light)
          : (Globals.darkSquare(this.square) ? Globals.ts_male_leopard_dark : Globals.ts_male_leopard_light);
    }

}
