package Blatt9;

import java.util.InputMismatchException;

/**
 * Die Klasse Main enthaelt das Hauptprogramm.
 *
 * Im Hauptprogramm wird zuerst der Benutzer gefragt,
 * wer das Spiel beginnen soll.
 *
 * Dann wird das Spiel gestartet.
 *
 */
public class Main {

    public static void main(String args[]) {
        Game game = new Game();
        Boolean error = true;
        while(error == true) {
            try {
                error = false;
                game.startGame(IO.readInt("Who has the first move? (0 = female, 1 = male)", 0, 1) == 0);
            } catch (InputMismatchException e) {
                System.out.println("Please input either 0 or 1");
                error = true;
            }
        }
    }

}
