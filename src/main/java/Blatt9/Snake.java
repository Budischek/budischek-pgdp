package Blatt9;

import java.util.Arrays;

public class Snake extends Predator {

    // Eine Schlange kann 9 Tage bzw. Spielrunden ohne Essen auskommen.
    // Die Deklaration darf entfernt (und der Wert z. B. direkt im Code
    // verwendet) werden.
    private static int withoutFood = 9;


    /**
     * Dem Konstruktor wird das Geschlecht des Tiers uebergeben.
     *
     */
    public Snake(boolean female) {
        super(female);
        this.daysLeft = withoutFood;
    }

    public Snake(boolean female, Position position, String square) {
        super(female, position, square);
        this.daysLeft = withoutFood;
    }

    @Override
    public Move[] possibleMoves() {
        char column = 'a';
        int row = 0;
        try {
            column = square.charAt(0);
            row = square.charAt(1) - '0';
        }
        catch (IndexOutOfBoundsException e) {
            return new Move[0];
        }

        Move[] moves = new Move[16];
        int nrMoves = 0;

        nrMoves = findVerticalMoves(moves, nrMoves);
        nrMoves = findHorizontalMoves(moves, nrMoves);

        moves = Arrays.copyOfRange(moves, 0, nrMoves);
        return moves;
    }

    private int findVerticalMoves(Move[] moves, int nrMoves) {
        char column = square.charAt(0);
        int row = square.charAt(1) - '0';
        int viable;

        int cOffset = 0;

        //Moves towards the top
        for(int rOffset = -1; rOffset > -8; rOffset--) {
            cOffset = cOffset^1;
            viable = isViableSquare(row + rOffset, (char) (column + cOffset));
            if(viable >= 0) {
                nrMoves++;
                moves[nrMoves - 1] = new Move(square, "" + (char) (column + cOffset) + (row + rOffset));
            }
            if(viable <= 0) {
                rOffset = -9;
            }
        }

        cOffset = 0;
        //Moves towards the bottom
        for(int rOffset = 1; rOffset < 8; rOffset++) {
            cOffset = cOffset^1;
            viable = isViableSquare(row + rOffset, (char) (column - cOffset));
            if(viable >= 0) {
                nrMoves++;
                moves[nrMoves - 1] = new Move(square, "" + (char) (column + cOffset) + (row + rOffset));
            }
            if(viable <= 0) {
                rOffset = 9;
            }
        }

        return nrMoves;
    }

    private int findHorizontalMoves(Move[] moves, int nrMoves) {
        char column = square.charAt(0);
        int row = square.charAt(1) - '0';
        int viable;
        int rOffset = 0;
        //Moves towards the left
        for(int cOffset = -1; cOffset > -8; cOffset--) {
            rOffset = rOffset ^ 1;
            viable = isViableSquare(row - rOffset, (char) (column + cOffset));
            if(viable >= 0) {
                nrMoves++;
                moves[nrMoves - 1] = new Move(square, "" + ((char) (column + cOffset)) + (row - rOffset));
            }
            if(viable <= 0) {
                cOffset = -9;
            }
        }

        rOffset = 0;

        //Moves towards the right
        for(int cOffset = 1; cOffset < 8; cOffset++) {
            rOffset = rOffset ^ 1;
            viable = isViableSquare(row + rOffset, (char) (column + cOffset));
            if(viable >= 0) {
                nrMoves++;
                moves[nrMoves - 1] = new Move(square, "" + ((char) (column + cOffset)) + (row + rOffset));
            }
            if(viable <= 0) {
                cOffset = 9;
            }
        }

        return nrMoves;
    }

    @Override
    public void consume() {
        daysLeft = withoutFood;
    }

    @Override
    public String toString(){
        return this.female
          ? (Globals.darkSquare(this.square) ? Globals.ts_female_snake_dark : Globals.ts_female_snake_light)
          : (Globals.darkSquare(this.square) ? Globals.ts_male_snake_dark : Globals.ts_male_snake_light);
    }

}
