package Blatt9.pgdp;

import java.util.LinkedList;
/**
 * Created by David on 16.12.2016.
 */
public class FruitBasket {
    private LinkedList<Fruit> fruits;

    public FruitBasket() {
        fruits = new LinkedList<Fruit>();
    }

    public void addFruit(Fruit f) {
        //Not sure how to handle the null case. Since we hadn't covered exceptions yet I chose
        //to not add a null value to the list without any indication of this.
        if(f != null) fruits.add(f);
    }

    public LinkedList<Fruit> getApples() {
        LinkedList<Fruit> returnList = new LinkedList<Fruit>();

        for (Fruit f : fruits) {
            if (f.isApple()) returnList.add(f);
        }

         return returnList;
    }

    public LinkedList<Fruit> getEqualOrLongerShelfLife(int n) {
        LinkedList<Fruit> returnList = new LinkedList<Fruit>();

        for (Fruit f : fruits) {
            if (f.shelfLife() >= n) returnList.add(f);
        }

        return returnList;
    }
}
