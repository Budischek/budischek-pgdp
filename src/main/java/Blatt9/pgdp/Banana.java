package Blatt9.pgdp;

/**
 * Created by David on 16.12.2016.
 */
public class Banana extends Fruit {
    @Override
    public boolean isBanana() {
        return true;
    }

    @Override
    public int shelfLife() {
        return 7;
    }
}