package Blatt9.pgdp;

/**
 * Created by David on 16.12.2016.
 */
public class Pineapple extends Fruit {
    @Override
    public boolean isPineapple() {
        return true;
    }

    @Override
    public int shelfLife() {
        return 20;
    }
}