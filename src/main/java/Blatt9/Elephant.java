package Blatt9;

import java.util.Arrays;

public class Elephant extends Vegetarian {

    /**
     * Dem Konstruktor wird das Geschlecht des Tiers uebergeben.
     *
     */
    public Elephant(boolean female) {
        super(female);
    }

    public Elephant(boolean female, Position position, String square) {
        super(female, position, square);
    }

    @Override
    public Move[] possibleMoves() {
        char column = 'a';
        int row = 0;
        try {
            column = square.charAt(0);
            row = square.charAt(1) - '0';
        }
        catch (IndexOutOfBoundsException e) {
            return new Move[0];
        }

        Move[] moves = new Move[15];
        int nrMoves = 0;
        char tmpC;
        int tmpR;

        nrMoves += findHorizontalMoves(moves, nrMoves);
        nrMoves += findVerticalMoves(moves, nrMoves);

        moves = Arrays.copyOfRange(moves, 0, nrMoves);
        return moves;
    }

    private int findVerticalMoves(Move[] moves, int nrMoves) {
        char column = square.charAt(0);
        int row = square.charAt(1) - '0';
        int viable;

        //Moves towards the bottom
        for(int rOffset = -1; rOffset > -8; rOffset--) {
            if(isViableSquare(row + rOffset, column) >= 0) {
                nrMoves++;
                moves[nrMoves - 1] = new Move(square, "" + column + (row + rOffset));
            }
            else {
                rOffset = -9;
            }
        }

        //Moves towards the top
        for(int rOffset = 1; rOffset < 8; rOffset++) {
            if(isViableSquare(row + rOffset, column) >= 0) {
                nrMoves++;
                moves[nrMoves - 1] = new Move(square, "" + column + (row + rOffset));
            }
            else {
                rOffset = 9;
            }
        }

        return nrMoves;
    }

    private int findHorizontalMoves(Move[] moves, int nrMoves) {
        char column = square.charAt(0);
        int row = square.charAt(1) - '0';
        //Moves towards the left
        for(int cOffset = -1; cOffset > -8; cOffset--) {
            if(isViableSquare(row, (char) (column + cOffset)) >= 0) {
                nrMoves++;
                moves[nrMoves - 1] = new Move(square, "" + ((char) (column + cOffset)) + row);
            }
            else {
                cOffset = -9;
            }
        }

        //Moves towards the right
        for(int cOffset = 1; cOffset < 8; cOffset++) {
            if(isViableSquare(row, (char) (column + cOffset)) >= 0) {
                nrMoves++;
                moves[nrMoves - 1] = new Move(square, "" + ((char) (column + cOffset)) + row);
            }
            else {
                cOffset = 9;
            }
        }

        return nrMoves;
    }


    @Override
    public String toString(){
        return this.female
          ? (Globals.darkSquare(this.square) ? Globals.ts_female_elephant_dark : Globals.ts_female_elephant_light)
          : (Globals.darkSquare(this.square) ? Globals.ts_male_elephant_dark : Globals.ts_male_elephant_light);
    }

}
