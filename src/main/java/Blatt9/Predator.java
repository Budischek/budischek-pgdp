package Blatt9;
/**
 * Klasse der Raubtiere.
 */
public class Predator extends Animal {

    public int daysLeft;

    /**
     * Dem Konstruktor wird das Geschlecht des Tiers uebergeben.
     *
     */
    public Predator(boolean female) {
        super(female);
    }

    public Predator(boolean female, Position position, String square) {
        super(female, position, square);
    }

    @Override
    protected int isViableSquare(int row, char col) {
        // '`' is the char you get when you decrease 'a' by 1
        if('`' < col && col < 'i' && row >= 1 && row <= 8) {
            Animal a = position.findAnimal("" + col + row);
            if (a == null) {
                return 1;
            }
            if((a.female != this.female && a instanceof Vegetarian)) {
                return 0;
            }
        }
        return -1;
    }

    /*
    Every Predator needs to override this (should be abstract)
     */
    public void consume() {
        //do nothing
    }

    @Override
    public void sunset() {
        if(--daysLeft < 0) position.kill(this);
    }
}
