package Blatt9;

import java.util.Arrays;
import java.util.InputMismatchException;

/**
 * Die Klasse Game fuehrt die Benutzerinteraktion durch.
 *
 */

public class Game {

    private Position pos;
    private Move[] currentMoves;
    private int nrMoves, turncounter;
    private Move predatorMove;
    private String status;
    private boolean currentPlayerFemale;

    private static String helpString = "-<Origin><Target>: Move a piece (e.g. e2e4). You can move up to 3 Vegetarians and 1 Predators per turn. \n" +
            "-<Square>: Print the valid moves for the chosen Animal. \n" +
            "-print: Print current boardstate. \n" +
            "-remainingdays: Print the amount of days without food until the Predators starve. \n" +
            "-moves: Output a list of your currently queued moves. \n" +
            "-editmoves: Edit your currently queued moves. \n" +
            "-possiblemoves: Prints a list of all possible turns in your current situation (not taking into account the turns you already queued up. \n" +
            "-endturn: Executes all moves you input before and ends your turn. \n";

    /**
     * Startet ein neues Spiel.
     * Der Benutzer wird ueber das Spielgeschehen informiert.
     *
     * Dazu gehoert auch die Information, wie lange die
     * einzelnen Raubtiere noch ohne Essen auskommen koennen.
     * Diese Information soll auf Anfrage oder immer angezeigt werden.
     *
     * Es soll ausserdem eine Moeglichkeit geben, sich alle Zuege
     * anzeigen zu lassen, die in der Spielsituation moeglich sind.
     *
     * Bei fehlerhaften Eingaben wird die Eingabe natuerlich wiederholt.
     *
     * Der Parameter spezifiziert, wer das Spiel beginnen darf.
     */
    public void startGame(boolean ladiesFirst){
        boolean validTurn;
        String input, player;
        pos = new Position();
        pos.reset(ladiesFirst ? 'W' : 'M');

        currentPlayerFemale = ladiesFirst;
        turncounter = 0;

        while(pos.theWinner() == 'X') {
            System.out.println(pos.toString());
            currentMoves = new Move[3];
            predatorMove = null;
            validTurn = false;
            nrMoves = 0;
            status = "";

            if(currentPlayerFemale) player = "W";
            else player = "M";
            while(!validTurn) {
                input = IO.readString("Player " + player + " what do you want to do? " + status + "(\"help\" for a list of commands)");
                switch (input) {
                    case "help":
                        System.out.println(helpString);
                        break;
                    case "print":
                        System.out.println(pos.toString());
                        break;
                    case "moves":
                        printMoveArray(currentMoves);
                        break;
                    case "possiblemoves":
                        printMoveArray(pos.allPossibleMoves(currentPlayerFemale));
                        break;
                    case "remainingdays":
                        printRemainingDays(pos.getLivingAnimals());
                        break;
                    case "endturn":
                        pos.applyMoves(combineMoveArray());
                        currentPlayerFemale = !currentPlayerFemale;
                        validTurn = true;
                        break;
                    case "editmoves":
                        if(nrMoves == 0 && predatorMove == null) {
                            System.out.println("You haven't entered any moves yet");
                            break;
                        }
                        int i = 0;
                        while(i < nrMoves) {
                            i++;
                            System.out.println(i + "- " + currentMoves[i - 1]);
                        }
                        if(predatorMove != null) {
                            i++;
                            System.out.println(i + "- " + predatorMove);
                        }
                        try{
                            int editMove = IO.readInt("Please enter a number to remove the corrosponding Move (Any other input will exit this menu)", 1, i);
                            if(editMove == i) predatorMove = null;
                            else {
                                currentMoves[editMove - 1] = currentMoves[nrMoves - 1];
                                currentMoves[nrMoves - 1] = null;
                                nrMoves--;
                            }
                        }
                        catch (InputMismatchException e) {
                            break;
                        }
                        break;
                    default:
                        if(input.length() == 4) {
                            System.out.println(inputMove(input));
                            break;
                        }
                        if(input.length() == 2) {
                            Animal tmp = pos.findAnimal(input);
                            if (tmp != null) {
                                printMoveArray(tmp.possibleMoves());
                            }
                        }
                        break;
                }
            }

            turncounter++;
            if(turncounter % 2 == 0) pos.sunset();
        }

        System.out.println(pos);

        switch (pos.theWinner()) {
            case 'M':
                System.out.println("Player M wins. Congratulations");
                break;
            case 'W':
                System.out.println("Player W wins. Congratulations");
                break;
            case 'N':
                System.out.println("It's a draw. Everybody is a winner");
                break;
            default:
                System.out.println("Error finishing up the game. Sorry for the inconvenience");
        }
    }

    private void printRemainingDays(Animal[] animals) {
        if(animals == null) return;
        String msg = "";
        animals = sortByDaysLeft(animals);
        //The output with ANSI formatting doesn't really work in this context.
        //Since .getClass() is not part of the permitted Java methods there is no easy way to convert the ANSI output
        //into plaintext
        boolean ansi = true;
        if(Globals.ts_male_penguin_light.equals(" P")) ansi = false;
        for(Animal a : animals) {
            if(a != null) {
                if(!ansi) msg += a.square + " ---" + a + " --- " + ((Predator) a).daysLeft + " Days remaining \n";
                else msg += a.square + " --- " + ((Predator) a).daysLeft + " Days remaining \n";
            }
        }

        System.out.println(msg);
    }

    private Animal[] sortByDaysLeft(Animal[] animals) {
        Predator[] sorted = new Predator[animals.length];
        int nrSorted = 0;
        for(Animal a : animals) {
            if(a != null && a instanceof Predator) {
                sorted[nrSorted] = (Predator) a;
                nrSorted++;
            }
        }

        boolean changed = true;
        Predator tmp = null;
        while(changed == true) {
            changed = false;
            for(int i = 1; i < nrSorted; i++) {
                if(sorted[i-1].daysLeft > sorted[i].daysLeft) {
                    tmp = sorted[i-1];
                    sorted[i-1] = sorted[i];
                    sorted[i] = tmp;
                    changed = true;
                }
            }
        }

        if(nrSorted > 0) {
            return Arrays.copyOfRange(sorted, 0, nrSorted);
        }
        else return new Animal[0];
    }

    private Move[] combineMoveArray() {
        currentMoves = Arrays.copyOfRange(currentMoves, 0, nrMoves);

        Move[] newMoves;
        int i = 0;

        if(predatorMove != null) {
            newMoves = new Move[nrMoves + 1];
            newMoves[0] = predatorMove;
            i = 1;
        }
        else newMoves = new Move[nrMoves];

        for(Move m : currentMoves) {
            newMoves[i] = m;
            i++;
        }

        return newMoves;
    }

    private String inputMove(String input) {
        if(nrMoves < 3 || predatorMove == null) {
            Move move = pos.isValidMove(input);
            if (move != null) {
                Animal from = pos.findAnimal(move.from);
                if (from.female == currentPlayerFemale) {
                    Move conflict = checkParallelMoves(move);
                    if(conflict == null) {
                        if (pos.findAnimal(move.from) instanceof Predator) {
                            if (predatorMove != null) {
                                return "You can only move 1 Predator per turn (\"moves\" for a list of your current moves, \"editmoves\" to change your current moves";
                            } else {
                                predatorMove = move;
                                return "Successfully added " + move + " to your current moves";
                            }
                        }
                        if (nrMoves < 3) {
                            currentMoves[nrMoves] = move;
                            nrMoves++;
                            return "Successfully added " + move + " to your current moves";
                        }
                        return "You can only move 3 Vegetarians per turn (\"moves\" for a list of your current moves, \"editmoves\" to change your current moves";
                    }
                    return "There is a conflict between this move and " + conflict + "(\"moves\" for a list of your current moves, \"editmoves\" to change your current moves";
                }
                return "You can only move your own animals";
            }
            return "Not a valid move";
        }
        return "You can only move 4 animals per turn (3 Vegetarians 1 Predator)";
    }

    private Move checkParallelMoves(Move move) {
        if(move == null) return move;
        if(predatorMove != null && (predatorMove.from.equals(move.from) || predatorMove.to.equals(move.to))) return predatorMove;

        for(Move m : currentMoves) {
            if(m != null) {
                if (m.from.equals(move.from) || m.to.equals(move.to)) {
                    return m;
                }
            }
        }

        return null;
    }

    private void printMoveArray(Move[] moves) {
        int count = 0;
        String msg = "";
        for(Move m : moves) {
            if(count > 5) {
                msg += "\n";
                count = 0;
            }
            if(m != null) {
                msg += m.toString() + "\t";
            }
            count++;
        }
        System.out.println(msg);
    }
}
