package Blatt9;

import java.util.Arrays;

/**
 * Die Klasse Position repraesentiert eine Spielsituation.
 *
 */
public class Position {

    /**
     * Die Tiere werden intern in einem Array gespeichert.
     * nrAnimals gibt an, wie viele Tiere auf dem Brett sind.
     * Diese sind in myAnimals an den Positionen 0 bis nrAnimals-1 enthalten.
     *
     * Es ist empfohlen, aber nicht vorgeschrieben, diese Attribute zu verwenden.
     *
     * Falls die beiden Attribute NICHT verwendet werden, muss die Ausgabe
     * der Spielposition unten entsprechend auf die verwendete Datenstruktur
     * angepasst werden. Die toString-Methode darf dabei nicht veraendert werden,
     * muss jedoch die selbe Rueckgabe liefern. D.h. es ist dann notwendig,
     * die Hilfsmethode boardRepresentation auf die verwendete Datenstruktur anzupassen.
     */
    private Animal[] myAnimals;
    private int nrAnimals;

    /**
     * Spieler, der als naechstes ziehen darf ('M' oder 'W').
     * Wird jedes Mal aktualisiert, wenn eine Seite ihre Zuege ausfuehrt.
     */
    private char next = 'W';


    /**
     * Stellt die Anfangsposition des Spiels her.
     * Der Parameter gibt an, welche Seite beginnt ('M' oder 'W').
     */
    public void reset(char movesNext) {
        myAnimals = new Animal[32];
        nrAnimals = 0;

        next = movesNext;

        char[][] setup = new char[][] {
                {'S','E','H','L','L','H','E','S'},
                {'P','R','R','R','R','R','R','P'},
                {'x','x','x','x','x','x','x','x'},
                {'x','x','x','x','x','x','x','x'},
                {'x','x','x','x','x','x','x','x'},
                {'x','x','x','x','x','x','x','x'},
                {'p','r','r','r','r','r','r','p'},
                {'s','e','h','l','l','h','e','s'}
        };

        Animal next = null;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                switch (setup[i][j]) {
                    case 'x': next = null;
                        break;
                    case 'S': next = new Snake(false, this, "" + J[j] + (8 - i));
                        break;
                    case 's': next = new Snake(true, this, "" + J[j] + (8 - i));
                        break;
                    case 'E': next = new Elephant(false, this, "" + J[j] + (8 - i));
                        break;
                    case 'e': next = new Elephant(true, this, "" + J[j] + (8 - i));
                        break;
                    case 'H': next = new Horse(false, this, "" + J[j] + (8 - i));
                        break;
                    case 'h': next = new Horse(true, this, "" + J[j] + (8 - i));
                        break;
                    case 'L': next = new Leopard(false, this, "" + J[j] + (8 - i));
                        break;
                    case 'l': next = new Leopard(true, this, "" + J[j] + (8 - i));
                        break;
                    case 'P': next = new Penguin(false, this, "" + J[j] + (8 - i));
                        break;
                    case 'p': next = new Penguin(true, this, "" + J[j] + (8 - i));
                        break;
                    case 'R': next = new Rabbit(false, this, "" + J[j] + (8 - i));
                        break;
                    case 'r': next = new Rabbit(true, this, "" + J[j] + (8 - i));
                        break;
                }

                if(next != null) {
                    myAnimals[nrAnimals] = next;
                    nrAnimals++;
                }
            }
        }
    }


    /**
     * Fuehrt die uebergebenen Zuege fuer einen der Spieler aus.
     * Die Reihenfolge soll keinen Unterschied machen.
     * Diese Methode geht davon aus, dass dies bereits ueberprueft wurde.
     *
     * Der Zustand des Spiels wird entsprechend angepasst, d. h. ein Spiel
     * kann von der Anfangsposition aus allein mittels Aufrufen dieser Methode
     * gespielt werden. Insbesondere wechselt durch den Aufruf das Zugrecht,
     * da M und W abwechselnd ziehen.
     *
     * @param move Array mit den Zuegen, die ausgefuehrt werden sollen.
     *
     */
    public void applyMoves(Move[] move){
        Animal from, to;
        for(Move m : move) {
            from = findAnimal(m.from);
            to = findAnimal(m.to);
            if(to != null) {
                kill(to);
                ((Predator) from).consume();
            }
            from.square = m.to;
        }

        if(next == 'W') next = 'M';
        else next = 'W';
    }


    /**
     * Ermittelt, ob/wer gewonnen hat.
     *
     * @return 'W' falls W gewonnen hat,
     *         'M' falls M gewonnen hat,
     *         'N' falls das Spiel unentschieden zu Ende ist,
     *         'X' falls das Spiel noch nicht zu Ende ist.
     *
     */
    public char theWinner() {
        int wVegetarianCount = 0;
        boolean wPredator = false;
        int mVegetarianCount = 0;
        boolean mPredator = false;

        for(int i = 0; i < nrAnimals; i++) {
            if(myAnimals[i].female) {
                if(myAnimals[i] instanceof Predator) wPredator = true;
                else wVegetarianCount++;
            }
            else {
                if(myAnimals[i] instanceof Predator) mPredator = true;
                else mVegetarianCount++;
            }
        }
        if(!mPredator && !wPredator) {
            if(wVegetarianCount > mVegetarianCount) return 'W';
            else if(mVegetarianCount > wVegetarianCount) return 'M';
            else return 'N';
        }

        if(!mPredator && mVegetarianCount == 0) {
            if(!wPredator && wVegetarianCount == 0) return 'N';
            else return 'W';
        }

        if(!wPredator && wVegetarianCount == 0) return 'M';

        //If there are no more Vegetarians left the game is decided by the last Predator who hasn't starved
        //To save the player from having to repeatedly pass this will recursively decide the winner.
        if(mVegetarianCount == 0 && wVegetarianCount == 0) {
            System.out.println("No more Vegetarians left, the game will now simulate until a Winner has been decided by starvation");
            System.out.println(toString());
            sunset();
            return theWinner();
        }

        return 'X';
    }

    public Move[] allPossibleMoves(Boolean female) {
        Move[] moves = new Move[0];

        for(int i = 0; i < nrAnimals; i++) {
            if(myAnimals[i].female == female) {
                moves = combineMoveArrays(moves, myAnimals[i].possibleMoves());
            }
        }

        return moves;
    }

    private Move[] combineMoveArrays(Move[] a, Move[] b) {
        Move[] combined = new Move[a.length + b.length];
        int nrCombined = 0;
        for(Move m : a) {
            if(m != null) {
                combined[nrCombined] = m;
                nrCombined++;
            }
        }

        for(Move m : b) {
            if(m != null) {
                combined[nrCombined] = m;
                nrCombined++;
            }
        }

        if(nrCombined > 0) return Arrays.copyOfRange(combined, 0, nrCombined);
        return new Move[0];
    }

    public Move isValidMove(String move) {
        String from, to;
        if(isValidSquare("" + move.charAt(0) + move.charAt(1)) && isValidSquare("" + move.charAt(2) + move.charAt(3))) {
            from = "" + move.charAt(0) + move.charAt(1);
            to = "" + move.charAt(2) + move.charAt(3);
            Animal animal = findAnimal(from);
            if(animal != null) {
                Move m = new Move(from, to);
                if(moveContains(animal.possibleMoves(), m) >= 0) return m;
            }
        }
        return null;
    }

    public Animal[] getLivingAnimals () {
        if(0 < nrAnimals) return Arrays.copyOfRange(myAnimals, 0, nrAnimals - 1);
        return null;
    }

    //replaces the Animal with the one that is currently last in the myAnimals Array and reduces the animals counter
    public void kill(Animal a) {
        int index = myAnimalsContains(a);
        if(index >= 0 && index < nrAnimals) {
            nrAnimals--;
            myAnimals[index] = myAnimals[nrAnimals];
            myAnimals[nrAnimals] = null;
            a.alive = false;

        }
    }

    //calls the sunset method of each animal that is still alive
    public void sunset() {
        Animal last = myAnimals[nrAnimals - 1];
        int count = 0, tmpNrAnimals;
        while(count < nrAnimals) {
            tmpNrAnimals = -1;
            while(tmpNrAnimals != nrAnimals && count < nrAnimals) {
                tmpNrAnimals = nrAnimals;
                myAnimals[count].sunset();
            }
            count++;
        }
    }

    public static boolean isValidSquare(String square) {
        if(jContains(square.charAt(0)) >= 0 && iContains(square.charAt(1)) >= 0) {
            return true;
        }
        return false;
    }

    public Animal findAnimal(String square) {
        for(Animal a : myAnimals) {
            if(a != null && a.square.equals(square)) return a;
        }

        return null;
    }

    private int moveContains(Move[] moves, Move move) {
        int count = 0;
        if(moves == null) return -1;
        for(Move m : moves) {
            if(m.equals(move)) return count;
            count++;
        }
        return -1;
    }

    //Returns the position of a in the myAnimals array or -1 if a isn't part of myAnimals
    private int myAnimalsContains(Animal a) {
        int i = 0;
        for(Animal animal : myAnimals) {
            if(animal.equals(a)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    //Returns the position of c in the J array or -1 if c isn't part of J
    private static int jContains(char c) {
        int i = 0;
        String input = c + "";
        for(String j : J) {
            if(j.equals(input)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    //Returns the position of c in the I array or -1 if c isn't part of I
    private static int iContains(char c) {
        int count = 0;
        int input = c - '0';
        for(int i : I) {
            if(i == input) {
                return count;
            }
            count++;
        }
        return -1;
    }

    // Ausgabe der Spielposition

    private static final int[] I = {8,7,6,5,4,3,2,1};
    private static final String[] J = {"a","b","c","d","e","f","g","h"};
    private static int toIndex(String s){return (s.charAt(0)-'a');}

    // Erzeugt eine 2D-Repraesentation der Spielposition.
    // Darf ggf. auf neue Datenstruktur angepasst werden (s.o.)
    // Die Rueckgabe ist ein zweidimensionales Array, welches
    // jedem Feld das darauf befindliche Tier (oder null) zuordnet.
    // Dabei laeuft der erste Index von der 'a'-Linie zur 'h'-Linie,
    // der zweite von der 1. zur 8. Reihe. D.h. wenn z.B. bei a[3][7]
    // ein Tier ist, ist das zugehörige Feld "d8" (vierter Buchstabe,
    // achte Zahl).
    public Animal[][] boardRepresentation(){
        Animal[][] a = new Animal[8][8];
        for (int i : I) {
            for (String j : J) {
                for (int k = 0; k < myAnimals.length; k++) {
                    if (null == myAnimals[k]) {break;}
                    if (myAnimals[k].square.equals(j+i)) {
                        a[toIndex(j)][i-1] = myAnimals[k];
                    }
                }
            }
        }
        return a;
    }


    @Override
    public String toString(){
        String str = "   a b c d e f g h\n";
        Animal[][] ani = boardRepresentation();
        for (int i : I) {
            str += (i+" ");
            for (String j : J) {
                if (null == ani[toIndex(j)][i-1]) {
                    str += (i+toIndex(j))%2==1 ? Globals.ts_empty_square_dark : Globals.ts_empty_square_light;
                } else {
                    str += ani[toIndex(j)][i-1].toString();
                }
            }
            str += " " + i + "\n";
        }
        str += "  a b c d e f g h\nIt is " + next + "'s turn.\n";
        return str;
    }

}
