package Blatt5;
import MiniJava.MiniJava;

public class Linja extends MiniJava {

    private static int[][] spielfeld = new int[8][6];

    /**
     * initialisiert das Spielfeld
     * Ziellinie fuer Spieler 1 ist Zeile 7
     * Ziellinie fuer Spieler -1 ist Zeile 0
     */
    private static void initSpiel() {
        for (int i = 0; i < spielfeld.length; i++) {
            if (i != 0 && i != spielfeld.length - 1) {
                spielfeld[i] = new int[]{-(12 - i + 1), 0, 0, 0, 0, 6 + i};
            }
            if (i == 0) {
                spielfeld[i] = new int[]{1, 2, 3, 4, 5, 6};
            }
            if (i == spielfeld.length - 1) {
                spielfeld[i] = new int[]{-6, -5, -4, -3, -2, -1};
            }
        }

    }

    /**
     *
     * @return formatiertes aktuelles Spielfeld
     */
    private static String output() {
        String tmp = "Spieler 1 spielt von oben nach unten\n"
                + "Spieler -1 spielt von unten nach oben\n";
        for (int i = 0; i < spielfeld.length; i++) {
            for (int j = 0; j < spielfeld[i].length; j++) {
                tmp = tmp + "\t" + spielfeld[i][j];
            }
            tmp = tmp + "\n";
        }
        return tmp;
    }

    /**
     * @return true, wenn die Eingabe stein im richtigen Wertebereich liegt und
     * zum Spieler gehoert; false, sonst
     */
    private static boolean gueltigeEingabe(int stein, int spieler, Boolean bonusRule) {
        Boolean valid = false;
        if(spieler == -1 || spieler == 1) {
            if(stein < 13 && stein > -13) {
                if(spieler > 0 && stein > 0) {
                    valid = true;
                }
                else if(spieler < 0 && stein < 0) {
                    valid = true;
                }
                else if(bonusRule) {
                    valid = true;
                }
            }
        }
        return valid;
    }

    /**
     * @param stein kann Werte -1 bis -12 und 1 bis 12 haben
     * @return gibt x-Koordinate von stein an Position 0 und die y-Koordinaten
     * von stein an Position 1 zurueck; falls stein nicht gefunden, wird {-1,-1}
     * zurueckgegeben
     */
    private static int[] findeStein(int stein) {
        for(int i = 0; i < spielfeld.length; i++) {
            for(int j = 0; j < spielfeld[i].length; j++) {
                if(spielfeld[i][j] == stein) {
                    return new int[]{i, j};
                }
            }
        }

        return new int[]{-1,-1};
    }

    /**
     * @param reihe hat Werte 0 bis 7
     * @return Anzahl der Steine in einer Reihe
     */
    private static int steineInReihe(int reihe) {
        if(reihe >= spielfeld.length||reihe <= 0) {
            return 99;
        }
        int count = 0;
        for(int i = 0; i < spielfeld[reihe].length; i++) {
            if(spielfeld[reihe][i] != 0) {
                count++;
            }
        }
        return count;
    }

    /**
     * Ueberprueft, ob der Zug zulaessig ist und fuehrt diesen aus, wenn er
     * zulaessig ist.
     *
     * @param vorwaerts == true: Zug erfolgt vorwaerts aus Sicht des
     * Spielers/Steins vorwearts == false: Zug erfolgt rueckwaerts aus Sicht des
     * Spielers/Steins
     * @return Rueckgabe -1: Zug nicht zulaessig Rueckgabe 0-5: Weite des
     * potentiellen naechsten Zugs (falls Folgezug folgt) Rueckgabe 6: Ziellinie
     * wurde genau getroffen (potentieller Bonuszug)
     *
     */
    private static int setzeZug(int stein, int weite, boolean vorwaerts) {
        int[] position = findeStein(stein);
        if(position[0] == -1) return -1;
        int row = position[0];
        int column = position[1];

        int direction = 1;
        if(stein < 0) direction = -1;

        int targetRow = row;
        if(vorwaerts) targetRow += weite*direction;
            else targetRow -= weite*direction;
        //a Stone cannot enter his own starting line/the opponents goal line
        if((stein > 0 && targetRow < 1) ||(stein < 0) && targetRow > 6) {
            return -1;
        }
        int stonesInRow = 0;
        if(targetRow > 0 && targetRow < 7) {
            stonesInRow = steineInReihe(targetRow);
            if(stonesInRow > 5) {
                return -1;
            }

            int targetColumn = findEmptyPosition(targetRow, direction);

            if(targetColumn < 0) return -1;

            spielfeld[targetRow][targetColumn] = spielfeld[row][column];
            spielfeld[row][column] = 0;
        }
        else if(targetRow == 0 || targetRow == 7){
            stonesInRow = 6;
            spielfeld[row][column] = 0;
        }
        else {
            spielfeld[row][column] = 0;
        }

        return stonesInRow;
    }

    /**
     * Player 1's stones fill up rows from right to left, Player 2's from left to right
     * @return -1 if there is no empty position otherwise the first empty position found
     */
    private static int findEmptyPosition(int targetRow, int direction) {
        int i = -1;
        if (direction == -1) i = 0;
        if (direction == 1) i = spielfeld[targetRow].length - 1;

        while(i < spielfeld[targetRow].length && i >= 0) {
            if(spielfeld[targetRow][i] == 0) {
                return i;
            }
            i -= direction;
        }

        return -1;
    }

    /**
     * @return true, falls die Bedingungen des Spielendes erfuellt sind, d.h.
     * alle Steine des einen Spielers sind an den Steinen des gegnerischen Spielers
     * vorbeigezogen
     *
     */
    private static boolean spielende() {
        int player1FurthestBack = 0;
        int player2FurthestBack = 7;
        for(int i = spielfeld.length - 1; i >= 0; i--) {
            for(int j = 0; j<spielfeld[i].length; j++) {
                if(spielfeld[i][j] > 0) player1FurthestBack = i;
            }
        }
        for(int i = 0; i < spielfeld.length; i++) {
            for(int j = 0; j < spielfeld[i].length; j++) {
                if(spielfeld[i][j] < 0) player2FurthestBack = i;
            }
        }

        if(player1FurthestBack > player2FurthestBack) {
            return true;
        }
        return false;
    }

    /**
     * zaehlt die Punkte der beiden Spieler und gibt das Ergebnis aus
     */
    private static void zaehlePunkte() {
        int player1points = 0;
        int player2points = 0;
        int player1stones = 0;
        int player2stones = 0;
        int value = 0; //this is the row value for Player 1
        for(int i = 0; i < spielfeld.length; i++) {
            switch (i) {
                case 0:
                    value = -5;
                    break;
                case 1:
                    value = -3;
                    break;
                case 2:
                    value = -2;
                    break;
                case 3:
                    value = -1;
                    break;
                case 4:
                    value = 1;
                    break;
                case 5:
                    value = 2;
                    break;
                case 6:
                    value = 3;
                    break;
                case 7:
                    value = 5;
                    break;
            }
            for(int j = 0; j < spielfeld[i].length; j++) {
                if(spielfeld[i][j] > 0) {
                    player1points += value;
                    player1stones++;
                }
                else if(spielfeld[i][j] < 0) {
                    player2points -= value;
                    player2stones++;
                }
            }
        }

        player1points += ((12-player1stones)*5);
        player2points += ((12-player2stones)*5);

        String output = "";
        if(player1points > player2points) output = "Spieler 1 gewinnt.";
        else if(player1points < player2points) output = "Spieler -1 gewinnt.";
        else output = "Unentschieden";
        write(output + "\n Spieler 1 Punkte: " + player1points + "||Spieler -1 Punkte: " + player2points);
    }

    /**
     * Spielablauf entsprechend Anfangszug, Folgezug, Bonuszug
     *
     * @param spieler ist 1 (Spielsteine 1 bis 12) oder -1 (Spielsteine -1 bis
     * -12)
     */
    private static void spielerZieht(int spieler, Boolean bonus) {
        int stone = 0;
        int player = spieler;
        int moves = 1;
        Boolean bonusRule = bonus;
        Boolean valid = false;

        //Starting move
        validateGamestate(moves, player, false, bonusRule);
        moves = move(moves, player, 1, bonusRule);
        System.out.println(output());
        //Follow up move
        if(moves < 6 && moves > 0) {
            validateGamestate(moves, player, false, bonusRule);
            moves = move(moves, player, 2, bonusRule);
            System.out.println(output());
        }
        //Bonus move
        if(moves == 6) {
            validateGamestate(1, player, true, bonusRule);
            moves = move(1,player, 3, bonusRule);
            System.out.println(output());
        }
    }

    /**
     * @param bonus This move is a bonus move (stone can be moved in both directions)
     * @param bonusRule The bonus Rule is active (a bonus move can be made with both players stones)
     * This method is used to reduce clutter in the spielerZieht method while also keeping the findValidMove method as clean as possible.
     *
     * We need this validation for 2 cases:
     * 1. There is no stone left to move that means the player got all his stones into the goal row and the game is done.
     * 2. There are stones left but no legal moves that means the game ends in a draw (this isn't part of the rules but since its an edge case I chose to copy the chess ruleset)
     *
     * Case 1 can happen when one player has one of his stones left in his starting line and the other player moves his last stone onto the goal line with either his Starting or Follow up move
     * This would leave him with a bonus move but no stone to move.
     */
    public static void validateGamestate(int moves, int player, boolean bonus, boolean bonusRule) {
        int result = 0;

        if(!(bonus && bonusRule)) {
            result = findValidMove(moves, player, bonus);
            if(result == 0) {
                write("Es gibt keinen gültigen Zug für Spieler " + player + ". Das Spiel endet als Patt.");
                System.exit(1);
            }
            else if(result == -1) {
                write("Spieler " + player + " hat alle Steine in der Zielzone");
                zaehlePunkte();
                System.exit(1);
            }
        }

        //We do not need to validate in case of a bonus move with bonus rules. Since the player can move any stone on the board there is no possible situation where cannot maake a move
        //what might happen is that the player has to help his opponent by moving a stone out of the opponents starting area but I think this is the way it is supposed to be.
    }

    /**
     *@return 1 = there is a valid move, 0 = no valid move, -1 = no stones left on board
     */
    public static int findValidMove(int moves, int player, boolean bonus) {
        //check if there is a stone left to be moved (e.g. moved the last stone directly onto the goal row, thus leaving you with a bonus move but no stone to move
        boolean stoneExists = false;

        for(int i = 0; i < spielfeld.length; i++) {
            for(int j = 0; j < spielfeld[i].length; j++) {
                if(spielfeld[i][j] > 0 && player > 0) {
                    if(checkMove(spielfeld[i][j], moves, player, bonus)) {
                        return 1;
                    }
                    stoneExists = true;
                }
                else if(spielfeld[i][j] < 0 && player < 0) {
                    if(checkMove(spielfeld[i][j], moves, player, bonus)) {
                        return 1;
                    }
                    stoneExists = true;
                }
            }
        }
        if(!stoneExists) {
            return -1;
        }

        return 0;
    }

    /**
     *
     * @return true if there is a valid move for the given stone, false if there is not
     */
    public static boolean checkMove(int stone, int moves, int player, boolean bonus) {
        if(!bonus) {
            int targetRow = findeStein(stone)[0]+(moves*player);
            //check if there is space in the target row or if target row is goal row
            if(targetRow >= spielfeld.length - 1 || targetRow <= 0 || steineInReihe(targetRow) < 6) {
                return true;
            }
        }
        else {
            //same as above but check in both directions.
            int targetRow1 = findeStein(stone)[0]+(moves*player);
            int targetRow2 = findeStein(stone)[0]-(moves*player);
            if(player == 1) {
                if(steineInReihe(targetRow1) < 6 || targetRow1 >= spielfeld.length - 1 || steineInReihe(targetRow2) < 6 || targetRow2 >= spielfeld.length - 1) {
                    return true;
                }
            }
            if(player == -1) {
                if(steineInReihe(targetRow1) < 6 || targetRow1 <= 0 || steineInReihe(targetRow2) < 6 || targetRow1 <= 0) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return same as setzeZug
     */
    public static int move(int moves, int player, int type, boolean bonusRule) {
        String moveType = "";
        Boolean bonusMove = false;
        int stone = 0;
        int result = 0;
        switch (type) {
            case 1: moveType = "Anfangszug";
                break;
            case 2: moveType = "Folgezug (" + moves + " lang)";
                break;
            case 3: moveType = "Bonuszug";
                bonusMove = true;
                break;
        }

        Boolean valid = false;
        Boolean forward = true;
        String output = "Spieler " + player + " " + moveType + " , welcher Stein wird gezogen?";

        while (!valid) {
            stone = readInt(output);
            valid = gueltigeEingabe(stone, player, bonusRule);
            if(valid) {
                if(bonusMove) {
                    valid = false;
                    String bonusOutput = "Spieler " + player + " " + moveType + ". Welche Richtung? (1 = Vorwärts, -1 = Rückwärts)";
                    while(!valid) {
                        int forwardInput = 0;
                        forwardInput = readInt(bonusOutput);
                        if(forwardInput == 1) {
                            forward = true;
                            valid = true;
                        }
                        else if(forwardInput == -1) {
                            forward = false;
                            valid = true;
                        }
                        else bonusOutput = "Spieler " + player + " " + moveType + ". Bitte geben sie einen gültigen Wert ein. Welche Richtung? (1 = Vorwärts, -1 = Rückwärts)";
                    }
                }
                result = setzeZug(stone, moves, forward);
                if(result < 0) {
                    valid = false;
                }
            }
            if(!valid) {
                output = "Spieler " + player + " " + moveType + " . Ungültige Eingabe. Welcher Stein wird gezogen?";
            }
        }
        return result;
    }


    public static void main(String args[]) {
        initSpiel();
        Boolean bonus = initBonus();
        System.out.println(output());
        int turn = dice(); //randomise the starting player
        while(!spielende()) {
            if(turn%2 == 0) {
                spielerZieht(1, bonus);
            }
            else spielerZieht(-1, bonus);
            turn++;
        }
        zaehlePunkte();
    }

    public static boolean initBonus() {
        int input = readInt("Do you want to play with the Bonus ruleset? (1 = yes, other number = no");
        if(input == 1) return true;
        return false;
    }
}
