package Blatt5;
import MiniJava.MiniJava;
/**
 *Decrypted code from task sheet:
 *decrypt("Mrn pnvnrwbcnw Jdopjknw bcnuuc Ajyqjnuj",9) => Die gemeinsten Aufgaben stellt Raphaela
 */
public class FunctionalCaesar extends MiniJava {
    private static String lowerAlphabet = "abcdefghijklmnopqrstuvwxyz";
    private static String upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static char shift(char c, int k) {
        int tmp = 0;
        int shift = k;
        for(int i=0; i<lowerAlphabet.length(); i++) {
            if (c == lowerAlphabet.charAt(i)) {
                tmp = i+shift;
                while(tmp < 0) tmp += lowerAlphabet.length();
                tmp %= lowerAlphabet.length();
                return lowerAlphabet.charAt(tmp);
            }
            if (c == upperAlphabet.charAt(i)) {
                tmp = i+shift;
                while(tmp < 0) tmp += upperAlphabet.length();
                tmp %= upperAlphabet.length();
                return upperAlphabet.charAt(tmp);
            }
        }
        return c;
    }

    public static String encrypt(String s, int k) {
        String input = s;
        int shift = k;
        String output = "";
        for(int i=0; i<input.length(); i++) {
            output += shift(input.charAt(i), shift);
        }
        return output;
    }

    public static String decrypt(String s, int k) {
        return encrypt(s, -(k % 26));
    }
    /**
     *Decrypted code from task sheet:
     *decrypt("Mrn pnvnrwbcnw Jdopjknw bcnuuc Ajyqjnuj",9) => Die gemeinsten Aufgaben stellt Raphaela
     */
    public static void main(String[] args) {
        String input = readString();
        int k = read();
        String out = encrypt(input, k);
        write(out);
    }

}

