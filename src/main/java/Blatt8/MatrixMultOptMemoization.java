package Blatt8;

import java.util.HashMap;
/**
 * Determine the amount of multiplications required if multiplying in the most effective order.
 */
public class MatrixMultOptMemoization {
    private static HashMap<Integer, HashMap<Integer, Integer>> map;

    public static int f(int[][] mm) {
        map = new HashMap<Integer, HashMap<Integer, Integer>>();
        return f(mm, 0, mm.length - 1);
    }

    public static int f(int[][] mm, int i, int j) {
        if(i == j || i > j) return 0;
        int value = 0;
        int min = Integer.MAX_VALUE;
        for(int count = i; count < j; count++) {
            value = 0;
            value += checkMap(mm, i, count);
            value += checkMap(mm, count + 1, j);
            value +=(mm[i][0] * mm[count][1] * mm[j][1]);
            if(value < min) min = value;
        }
        return min;
    }

    private static int checkMap(int[][] mm, int i, int j) {
        int value = 0;
        int tmp = 0;
        if(map.containsKey(i)) {
            if(map.get(i).containsKey(j)) {
                value += map.get(i).get(j);
            }
            else {
                tmp = f(mm, i, j);
                map.get(i).put(j, tmp);
                value = tmp;
            }
        }
        else {
            map.put(i, new HashMap<Integer, Integer>());
            tmp = f(mm, i, j);
            map.get(i).put(j, tmp);
            value = tmp;
        }

        return value;
    }

}
