package Blatt8;

public class SymmetricStack {

    private int[] data;
    private int first;
    private int last;

    public SymmetricStack() {
        data = new int[10];
        first = -1;
        last = first;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getLast() {
        return last;
    }

    public void setLast(int last) {
        this.last = last;
    }

    public int[] getData() {
        return data;
    }

    public void setData(int[] data) {
        this.data = data;
    }

    public int getNumberOfElements() {
        if(isEmpty()) return 0;

        if(first == last) return 1;

        //take the amount of free space between first and last and subtract it from the total length
        if(first > last) return data.length - (first - last - 1);

        //take the amount of free space left of first and right of last and subtract it from the total length
        return data.length - (data.length - last - 1) - first;
    }

    public void increase() {
        if(isFull()) {
            int[] newData = new int[data.length * 2];
            if(newData.length == 0) newData = new int[1];
            if(!isEmpty()) {
                int positionNew = data.length / 2;
                int positionOld = first - 1;

                for (int i = 0; i < data.length; i++) {
                    if (++positionOld >= data.length) positionOld = 0;
                    newData[positionNew + i] = data[positionOld];
                }

                setFirst(positionNew);
                setLast(positionNew + data.length - 1);
            }
            setData(newData);
        }
    }

    public void decrease() {
        if(isEmpty()) {
            setData(new int[data.length/2]);
            if(data.length == 0) data = new int[1];
        }
        else if(getNumberOfElements() <= (data.length / 4)){
            int[] newData = new int[data.length / 2];
            if(newData.length == 0) newData = new int[1];
            int positionNew = data.length / 8;
            int positionOld = first - 1;
            int tmpNumberOfElements = getNumberOfElements();
            setLast(positionNew + tmpNumberOfElements - 1);
            for(int i = 0; i<tmpNumberOfElements; i++) {
                if(++positionOld >= data.length) positionOld = 0;
                newData[positionNew + i] = data[positionOld];
            }

            setFirst(positionNew);


            setData(newData);
        }
    }

    public boolean isEmpty() {
        if(first == -1) return true;
        return false;
    }

    public boolean isFull() {
        if( (first - last) == 1 || ((last - first) == data.length-1 && data.length > 0 && first != -1)) return true;
        return false;
    }

    public void prepend(int x) {
        if(isFull()) increase();

        int position;

        if(isEmpty()) {
            position = data.length / 2;
            setLast(position);
        }
        else {
            position = first - 1;
            if(position == -1) position = data.length-1;

        }

        data[position] = x;
        setFirst(position);

        //According to task: "Falls das Array data voll ist, dann wird dieses mit der Methode increase vergrößert.".
        //It is unclear if the array size should only be increased if the Array is full before the new value is inserted
        //if(isFull()) increase();
    }

    public void append(int x) {
        if(isFull()) increase();

        int position;

        if(isEmpty()) {
            position = data.length / 2;
            setFirst(position);
        }
        else {
            position = last + 1;
            if(position == data.length) position = 0;

        }

        data[position] = x;
        setLast(position);

        //According to task: "Falls das Array data voll ist, dann wird dieses mit der Methode increase vergrößert.".
        //It is unclear if the array size should only be increased if the Array is full before the new value is inserted
        //if(isFull()) increase();
    }

    public void removeFirst() {
        if(!isEmpty()) {
            if(first == last) {
                setFirst(-1);
                setLast(-1);
            }
            else {
                if(++first >= data.length) setFirst(0);
            }
        }
        decrease();
    }

    public void removeLast() {
        if(!isEmpty()) {
            if(first == last) {
                setFirst(-1);
                setLast(-1);
            }
            else {
                if(--last <= 0) setLast(data.length - 1);
            }
        }
        decrease();
    }

    @Override
    public String toString() {
        String out = "";
        for (int i = 0; i < data.length; i++) {
            if (first <= last && (i < first || i > last))
                out += "* ";
            if (first <= last && i > first && i < last)
                out += " " + data[i];
            if (first > last && i > last && i < first)
                out += "* ";
            if (first > last && (i > first || i < last))
                out += " " + data[i];
            if (i == first)
                out = out + "(" + data[i];
            if (i == last)
                out += (first == last ? "" : " " + data[i]) + ")";
        }
        return out;
    }
}
