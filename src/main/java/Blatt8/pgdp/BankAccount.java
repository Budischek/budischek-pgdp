package Blatt8.pgdp;

/**
 * Created by David on 14.12.2016.
 */
public class BankAccount {
    private int bankaccount;
    private String firstname;
    private String surname;
    private Money balance;

    public BankAccount(int accountnumber, String fname, String sname) {
        this.bankaccount = accountnumber;
        this.firstname = fname;
        this.surname = sname;
        balance = new Money();
    }

    public int getAccountnumber() {
        return bankaccount;
    }

    public String getSurname() {
        return surname;
    }

    public String getFirstname() {
        return firstname;
    }

    public Money getBalance() {
        return balance;
    }

    public void addMoney(Money m) {
        balance = balance.addMoney(m);
    }

    public String toString() {
        return "Account: " + bankaccount + " Surame: " + surname + " Firstname: " + firstname + " Balance: " + balance;
    }
}
