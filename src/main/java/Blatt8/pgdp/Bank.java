package Blatt8.pgdp;

/**
 * Created by David on 14.12.2016.
 */
public class Bank {
    private BankAccountList accounts;

    public int newAccount(String firstname, String lastname) {
        if(firstname == "" || lastname == "") return Integer.MIN_VALUE;
        if(accounts == null) {
            accounts = new BankAccountList();
            accounts.info = new BankAccount(1, firstname, lastname);
            return 1;
        }
        BankAccountList account = accounts;
        while(account.next != null) {
            account = account.next;
        }
        if(account.info.getAccountnumber() < Integer.MAX_VALUE) {
            BankAccountList newAccount = new BankAccountList();
            newAccount.info = new BankAccount(account.info.getAccountnumber() + 1, firstname, lastname);
            account.next = newAccount;
            return newAccount.info.getAccountnumber();
        }
        return Integer.MIN_VALUE;
    }

    public void removeAccount(int accountnumber) {
        BankAccountList account = accounts;
        boolean foundAccount = false;
        if(account != null && account.info.getAccountnumber() == accountnumber) {
            accounts = account.next;
            account = accounts;
        }
        while(account != null && account.next != null && !foundAccount) {
            if(account.next.info.getAccountnumber() == accountnumber) foundAccount = true;
            else account = account.next;
        }

        if(foundAccount) account.next = account.next.next;
    }

    public Money getBalance(int accountnumber) {
        BankAccountList account = accounts;
        while(account != null) {
            if(account.info.getAccountnumber() == accountnumber) return account.info.getBalance();
            if(account.next != null) account = account.next;
            else account = null;
        }
        return null;
    }

    public boolean depositOrWithdraw(int accountnumber, Money m) {
        BankAccountList account = accounts;
        while(account != null) {
            if(account.info.getAccountnumber() == accountnumber) {
                account.info.addMoney(m);
                return true;
            }
            if(account.next != null) account = account.next;
            else account = null;
        }
        return false;
    }

    public boolean transfer(int from, int to, Money m) {
        if(getBalance(from) != null) {
            if(depositOrWithdraw(to, m)) {
                Money subtract = new Money(m.getCent() * -1);
                if(depositOrWithdraw(from, subtract)) {
                    return true;
                }
                else depositOrWithdraw(to, subtract);
            }
        }
        return false;
    }

    private class BankAccountList {
        public BankAccount info;
        public BankAccountList next;
    }

}
