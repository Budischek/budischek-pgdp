package Blatt8.pgdp;

/**
 * Created by David on 13.12.2016.
 */
public class Money {
    private int cent;

    public Money() {
        this(0);
    }

    public Money(int cent) {
        this.cent = cent;
    }

    public int getCent() {
        return cent;
    }

    public Money addMoney(Money m) {
        return new Money(cent + m.getCent());
    }

    public String toString() {
        int cent = getCent();
        String sign = "";
        if(cent < 0) {
            cent *=  -1;
            sign = "-";
        }
        String decimals = "" + cent % 100;

        if(decimals.length() == 1) { decimals = "0" + decimals; }
        return sign + cent / 100 + "," + decimals + " Euro";
    }
}
