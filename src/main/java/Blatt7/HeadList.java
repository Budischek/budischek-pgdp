package Blatt7;

public class HeadList {

    Entry head;

    /**
     * constructor empty HeadList
     */
    public HeadList() {
        head = null;
    }

    /**
     * Appends a new element with value info to the end of this list
     * @param info value of the new element
     */
    public void add(int info) {
        Entry tmp = findLast(head);
        if (tmp != null) {
            tmp.next = new Entry(head, null, info);
        }
        else {
            tmp = new Entry(null, null, info);
            tmp.first = tmp;
            head = tmp;
        }
    }

    /**
     * Removes and returns the element at position index from this list.
     * @param index position of the element that is removed
     * @return value of the removed element
     */
    public int remove(int index) {
        Entry tmp;
        Entry removed;
        if(index < 0 || head == null) return Integer.MIN_VALUE;

        tmp = head;

        if(index == 0) {
            removed = head;
            head = tmp.next;
            setHead(head);
            return removed.elem;
        }

        for(int i = 0; i < index-1; i++) {
            if(tmp.next != null) {
                tmp = tmp.next;
            }
            else return Integer.MIN_VALUE;
        }

        if(tmp.next != null) {
            removed = tmp.next;
            tmp.next = removed.next;
            return removed.elem;
        }

        return Integer.MIN_VALUE;
    }

    /**
     * sets the head of each list element to newHead
     * @param newHead reference to the new head
     */
    private void setHead(Entry newHead) {
        Entry tmp;
        if (head != null) {
            tmp = head;
            while (tmp != null) {
                tmp.first = newHead;
                tmp = tmp.next;
            }
        }
    }

    /**
     * reverse the list
     * example: [1,2,3,4,5] --> [5,4,3,2,1], [] --> [], [1] --> [1]
     */
    public void reverse() {
        Entry newHead = findLast(head);

        if(newHead != null && newHead != head) {
            Entry newLast = newHead;
            Entry tmp = head;
            while(newLast != null && head != newLast) {
                tmp = head;
                if (head.next != newLast) {
                    while (tmp.next != newLast) {
                        tmp = tmp.next;
                    }
                }
                tmp.next = null;
                newLast.next = tmp;
                newLast = tmp;
            }

            head = newHead;
            setHead(head);
        }
    }

    private Entry findLast(Entry head) {
        Entry tmp = null;
        if (head != null) {
            tmp = head;
            while (tmp.next != null) {
                tmp = tmp.next;
            }
        }
        return tmp;
    }

    @Override
    public String toString() {
        String out = "[";
        if (head != null) {
            out += head.elem;
            Entry tmp = head.next;
            while (tmp != null) {
                out = out + "," + tmp.elem;
                tmp = tmp.next;
            }
        }
        out += "]";
        return out;
    }

    public static void main(String[] args) {
        HeadList l = new HeadList();
        System.out.println("empty list: " + l);
        // Test implementation

    }

    class Entry {

        Entry first;
        Entry next;
        int elem;

        public Entry(Entry first, Entry next, int elem) {
            this.first = first;
            this.next = next;
            this.elem = elem;
        }

    }

}
 
