package Blatt7;

import MiniJava.MiniJava;

public class DameSpiel extends MiniJava {

    private int nrRows, nrColumns; // Board dimensions
    private boolean[][] board;     // true = queen, false = empty
    private boolean whiteToMove;   // Whose turn it is
    private String white, black;   // Players' names
    private boolean[][] validPositions;
    private boolean surrender;


    /**
     * Der Konstruktor registriert Spielernamen fuer Weiss und Schwarz.
     *
     * @param white Name des als 'Weiss' bezeichneten Spielers
     * @param black Name des als 'Schwarz' bezeichneten Spielers
     */
    public DameSpiel(String white, String black){
        this.white = white;
        this.black = black;
    }


    /**
     * Gibt das Spielbrett aus.
     */
    private void printBoard(){
        for (int j = board[0].length - 1; j >= 0; j--) {
            System.out.print("\n " + (1 + j));
            for (int i = 0; i < board.length; i++) {
                System.out.print(board[i][j] ? " X" : " -");
            }
        }
        System.out.print("\n  ");
        for (int i = 1; i <= board.length; i++) {
            System.out.print(" " + i);
        }
        System.out.println("\n" + (whiteToMove ? white : black) + " ist am Zug.");
    }


    /**
     * Initialisiert das Spielbrett ueberall mit false.
     * Dazu wird (ggf. neuer) Speicher allokiert.
     */
    private void initBoard(){
        board = new boolean[nrColumns][nrRows];
        for(int c = 0; c < nrColumns; c++) {
            for(int r = 0; r < nrRows; r++) {
                board[c][r] = false;
            }
        }
    }


    /**
     * Ermittelt die Groesse des Spielbretts gemaess den Spielregeln.
     * Das Ergebnis der Abfrage wird in den Attributen nrRows und nrColumns abgelegt.
     */
    private void determineBoardSize(){
        nrColumns = 0;
        nrRows = 0;

        while(nrColumns == 0) {
            int tmp = readInt(white + " please select one of the following Numbers as Width for your field. [5, 6, 7, 8]");
            if(5 <= tmp && tmp <= 8) {
                nrColumns = tmp;
            }
        }
        while(nrRows == 0) {
            int tmp = readInt(black + " please select one of the following Numbers as length for your field. [" + (nrColumns-1) + "," + nrColumns + "," + (nrColumns+1) + "]");
            if(nrColumns-1 <= tmp && tmp <= nrColumns+1) {
                nrRows = tmp;
            }
        }
    }

    /**
     * Ermittelt, wer anfaengt zu ziehen.
     * Das Ergebnis der Abfrage wird im Attribut whiteToMove abgelegt.
     */
    private void determineFirstPlayer(){
        Boolean valid = false;
        while(!valid) {
            int tmp = readInt(white + " please decide who starts the game. 0 = " + white + " 1 = " + black);
            if(tmp == 0) {
                valid = true;
                whiteToMove = true;
            }
            else if(tmp == 1) {
                valid = true;
                whiteToMove = false;
            }
        }
    }


    /**
     * Fuehrt den Zug aus.
     *
     * @param move der auszufuehrende Zug!
     */
    private void applyMove(int move){
        int mRow;
        int mCol;
        if(move == -1) {
            surrender = true;
        }
        else {
            mCol = move/10;
            mRow = move%10;

            if(mCol > 0 && mCol <= nrColumns && mRow > 0 && mRow <= nrRows && !validPositions[mCol-1][mRow-1] ) {
                board[mCol-1][mRow-1] = true;
                whiteToMove = !whiteToMove;
            }

        }

    }

    /**
     * Creates a Board that shows which tiles are "attacked" by existing Queens
     * true = attacked/claimed by Queen
     * false = open tile
     */
    private void generateValidPositions() {
        validPositions = new boolean[nrColumns][nrRows];
        for(int c = 0; c < nrColumns; c++) {
            for(int r = 0; r < nrRows; r++) {
                validPositions[c][r] = false;
            }
        }

        for(int c = 0; c < nrColumns; c++) {
            for(int r = 0; r < nrRows; r++) {
                if(board[c][r]) {
                    validPositions[c][r] = true;
                    setColumnTrue(validPositions, c);
                    setRowTrue(validPositions, r);
                    setDiagonalsTrue(validPositions, c, r);
                }
            }
        }
    }

    private void setColumnTrue(boolean[][] tmpBoard, int column) {
        for(int r = 0; r < nrRows; r++) {
            tmpBoard[column][r] = true;
        }
    }

    private void setRowTrue(boolean[][] tmpBoard, int row) {
        for(int c = 0; c < nrColumns; c++) {
            tmpBoard[c][row] = true;
        }
    }

    private void setDiagonalsTrue(boolean[][] tmpBoard, int column, int row) {
        int count = 0;
        int columnOffset = 1;
        int rowOffset = 1;
        int tmpC, tmpR;
        while(count < 4) {
            tmpC = column;
            tmpR = row;
            while(tmpC >= 0 && tmpC < nrColumns && tmpR >= 0 && tmpR < nrRows) {
                tmpBoard[tmpC][tmpR] = true;
                tmpC += columnOffset;
                tmpR += rowOffset;
            }
            switch (count) {
                case 0: rowOffset = -1;
                    break;
                case 1: columnOffset = -1;
                    break;
                case 2: rowOffset = 1;
                    break;
            }

            count ++;
        }
    }


    /**
     * Startet die Hauptschleife des Spiels
     * mit der Abfrage nach Zuegen.
     * Die Schleife wird durch Eingabe von -1 beendet.
     */
    private void mainLoop(){
        String outputString;
        boolean toMove;
        surrender = false;
        boolean movesLeft = true;

        while(!surrender && movesLeft) {
            movesLeft = false;
            generateValidPositions();
            for(int c = 0; c < nrColumns; c++) {
                for(int r = 0; r < nrRows; r++) {
                    if(!validPositions[c][r]) {
                        movesLeft = true;
                        c = nrColumns;
                        r = nrRows;
                    }
                }
            }

            if(whiteToMove) {
                outputString = white + " where do you want to place your Queen? (-1 to surrender)";
            }
            else {
                outputString = black + " where do you want to place your Queen? (-1 to surrender)";
            }

            toMove = whiteToMove;

            while(!surrender && whiteToMove == toMove && movesLeft) {
                applyMove(readInt(outputString));
            }
            printBoard();
        }
    }


    /**
     * Informiert die Benutzerin ueber den Ausgang des Spiels.
     * Speziell: Wer hat gewonnen (Weiss oder Schwarz)?
     */
    private void reportWinner(){
        String output = "No valid moves left \n";

        if(surrender) output = "Surrender \n";

        if(whiteToMove) write(output + black + " wins");
        else write(output + white + " wins");
    }

    /**
     * Startet das Spiel.
     */
    public void startGame(){
        determineBoardSize();
        initBoard();
        determineFirstPlayer();
        printBoard();
        mainLoop();
        reportWinner();
    }


    public static void main(String[] args) {
        DameSpiel ds = new DameSpiel("Weiß", "Schwarz");
        ds.startGame();
    }

}
