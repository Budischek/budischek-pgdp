package Blatt11;

/**
 * Created by david on 21.01.2017.
 */
public class IllegalCharExc extends Exception{
    private final char used;

    public IllegalCharExc(char used) {
        this.used = used;
    }

    @Override
    public String toString() {
        String msg = "Illegal character: ";
        switch (used) {
            case '\n':
                msg += "line break (\\n)";
                break;
            case '\t':
                msg += "tab (\\t)";
                break;
            case '\r':
                msg += "carriage return (\\r)";
                break;
            case '\f':
                msg += "form feed (\\f)";
                break;
            case '\b':
                msg += "backspace (\\b)";
                break;
            default:
                msg += used;
                break;
        }

        return msg;
    }
}
