package Blatt11;

/**
 * Created by david on 21.01.2017.
 */
public class NotEnoughUpper extends NotEnoughLetter{
    public NotEnoughUpper(int should, int is) {
        super(should, is);
    }

    @Override
    public String toString() {
        return "Password doesn't contain enough uppercase characters. You entered " + is + " uppercase characters. Please enter " + should + " uppercase characters";
    }
}
