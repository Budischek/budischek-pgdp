package Blatt11;
/**
 * Created by david on 21.01.2017.
 */
public class NotEnoughNumber extends NotEnoughExc{
    public NotEnoughNumber(int should, int is) {
        super(should, is);
    }

    @Override
    public String toString() {
        return "Password doesn't contain enough numbers. You entered " + is + " numbers. Please enter " + should + " numbers";
    }
}