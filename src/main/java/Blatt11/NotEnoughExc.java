package Blatt11;

/**
 * Created by david on 21.01.2017.
 */
public class NotEnoughExc extends Exception{
    protected final int should;
    protected final int is;

    public NotEnoughExc(int should, int is) {
        this.should = should;
        this.is = is;
    }
}
