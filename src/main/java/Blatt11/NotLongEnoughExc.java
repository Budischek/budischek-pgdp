package Blatt11;

/**
 * Created by david on 21.01.2017.
 */
public class NotLongEnoughExc extends Exception{
    private final int should;
    private final int is;

    public NotLongEnoughExc(int should, int is) {
        this.should = should;
        this.is = is;
    }

    @Override
    public String toString() {
        return "Password not long enough. You entered " + is + " characters. Please enter " + should + " characters";
    }
}
