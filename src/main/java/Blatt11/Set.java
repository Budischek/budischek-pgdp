package Blatt11;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by david on 16.01.17.
 */
public class Set<T> implements Iterable<T>{
    private final Entry<T> entry;

    public Set() {
        entry = null;
    }

    public Set(Entry<T> entry) {
        this.entry = entry;
    }

    public Set<T> add(T e) {
        if(e == null) throw new NullPointerException();
        Entry<T> next = entry;
        Entry<T> current = new Entry<T>(e);
        Entry<T> tmp;

        while(next != null) {
            //t is already part of this set
            if(e.equals(next.value)) return this;
            current = next.setNext(current);
            next = next.next;
        }

        return new Set<T>(current);
    }

    public Set<T> remove(Object o) {
        Entry<T> next = entry;
        Entry<T> current = null;
        Entry<T> tmp;

        while(next != null) {
            if(o == null ? next.value==null : o.equals(next.value)) {
                next = next.next;
            }
            else {
                current = next.setNext(current);
                next = next.next;
            }
        }

        return new Set<T>(current);
    }

    public int size() {
        int count = 0;
        Entry<T> current = entry;

        while(current != null) {
            count++;
            current = current.next;
        }

        return count;
    }

    public boolean contains(Object o) {
        Entry<T> current = entry;

        while(current != null) {
            if(o==null ? current.value==null : o.equals(current.value)) return true;
            current = current.next;
        }

        return false;
    }

    @Override
    public String toString() {
        if(entry == null) return "{}";

        Entry<T> current = entry.next;
        String message = "{" + entry;

        while(current != null) {
            message += "," + current.value;
            current = current.next;
        }

        return message + "}";
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Set) {
            Set<T> otherSet = (Set) obj;
            Entry<T> current = entry;
            while(current != null) {
                if(!otherSet.contains(current.value)) return false;
                current = current.next;
            }
            return this.size() == ((Set) obj).size();
        }

        return false;
    }

    public Iterator<T> iterator() {
        return new SetIterator();
    }

    private class SetIterator implements Iterator<T> {
        private Entry<T> current;

        public SetIterator() {
            //The iterator has to start before the first element so the first call of next returns the first value;
            this.current = new Entry<T>(null, Set.this.entry);
        }

        public boolean hasNext() {
            return (current != null && current.next != null);
        }

        public T next() {
            if(this.hasNext()) {
                current = current.next;
                return current.value;
            }
            throw new NoSuchElementException();
        }

        //The optional remove() method is not implemented because it wouldn't make sense in this context (this iterator is supposed to iterate through one Set but remove() would create a new Set)
    }

    private class Entry<T> {
        public final T value;
        public final Entry next;

        public Entry() {
            value = null;
            next = null;
        }

        public Entry(T value) {
            this.value = value;
            next = null;
        }

        public Entry(T value, Entry<T> next) {
            this.value = value;
            this.next = next;
        }

        public Entry<T> setNext(Entry<T> next) {
            return new Entry(value, next);
        }

        public Entry<T> setValue(T value) {
            return new Entry(value, next);
        }

        @Override
        public boolean equals(Object obj) {
            if(obj instanceof Entry) {
                //If value is null we can check by reference if obj.value is also null, same for reference
                if((value != null && value.equals(((Entry) obj).value)) || (value == ((Entry) obj).value)) {
                    if((next != null && next.equals(((Entry) obj).next)) || (next == ((Entry) obj).next)) {
                        return true;
                    }
                }
            }
            return false;
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }
}
