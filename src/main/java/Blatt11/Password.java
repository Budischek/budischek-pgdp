package Blatt11;

/**
 * Created by david on 21.01.2017.
 */
public class Password {
    private final int nrUpperShould, nrLowerShould, nrSpecialShould, nrNumbersShould, lengthShould;
    private final char[] illegalChars;

    public Password(int nrUpperShould, int nrLowerShould, int nrSpecialShould, int nrNumbersShould, int lengthShould, char[] illegalChars) {
        this.nrUpperShould = nrUpperShould;
        this.nrLowerShould = nrLowerShould;
        this.nrSpecialShould = nrSpecialShould;
        this.nrNumbersShould = nrNumbersShould;
        this.lengthShould = lengthShould;
        this.illegalChars = illegalChars;
    }

    public static void main(String[] args) {
        Password pwd = new Password(2, 2, 4, 1, 10, new char[]{'A'});
        try {
            pwd.checkFormat("PAssw0rd()\n");
        }
        catch(Exception e) {
            System.out.println(e);
        }
    }


    public void checkFormat (String pwd) throws IllegalCharExc, NotEnoughExc, NotLongEnoughExc{
        int upper = 0, lower = 0, special = 0, numbers = 0, length = 0;
        char c;
        for(int i = pwd.length(); i > 0; i--) {
            c = pwd.charAt(i-1);
            if(isIllegal(c)) throw new IllegalCharExc(c);
            else if(isLower(c)) lower++;
            else if(isUpper(c)) upper++;
            else if(isNumber(c)) numbers++;
            else special++;
            length++;
        }

        if(upper < nrUpperShould) throw new NotEnoughUpper(nrUpperShould, upper);
        if(lower < nrLowerShould) throw new NotEnoughLower(nrLowerShould, lower);
        if(special < nrSpecialShould) throw new NotEnoughSpecial(nrSpecialShould, special);
        if(length < lengthShould) throw new NotLongEnoughExc(lengthShould, length);
        if(numbers < nrNumbersShould) throw new NotEnoughNumber(nrNumbersShould, numbers);
    }

    //This method only checks for german characters. To check for other languages we would need Character.getType
    private boolean isLower(char c) {
        return (c >= 'a' && c <= 'z') || c == 'ö' || c == 'ä' || c == 'ü';
    }

    private boolean isUpper(char c) {
        return (c >= 'A' && c <= 'Z') || c == 'Ö' || c == 'Ä' || c == 'Ü';
    }

    private boolean isNumber(char c) {
        return c <= '9' && c >= '0';
    }

    private boolean isIllegal(char c) {
        for(char illegal : illegalChars) {
            if(illegal == c) return true;
        }

        return false;
    }
}
