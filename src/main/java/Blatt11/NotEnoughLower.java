package Blatt11;

/**
 * Created by david on 21.01.2017.
 */
public class NotEnoughLower extends NotEnoughLetter{
    public NotEnoughLower(int should, int is) {
        super(should, is);
    }

    @Override
    public String toString() {
        return "Password doesn't contain enough lowercase characters. You entered " + is + " uppercase characters. Please enter " + should + " lowercase characters";
    }
}