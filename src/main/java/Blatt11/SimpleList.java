package Blatt11;

/**
 * Created by david on 16.01.17.
 */
public class SimpleList<T> {
    private final Entry<T> entry;

    public SimpleList() {
        entry = null;
    }

    public SimpleList(Entry<T> entry) {
        this.entry = entry;
    }

    public Entry<T> contains(T value) {
        Entry<T> current = entry;
        while(current != null) {
            if(current.value == value) return current;
            current = current.next;
        }

        return null;
    }

    public SimpleList<T> add(T value) {
        Entry<T> newLast = new Entry<T>(value);
        SimpleList<T> newList = new SimpleList<T>(newLast);
        if(entry == null) return newList;

        Entry<T> previous = null;
        Entry<T> newPrevious = newLast;
        while(previous != entry) {
            Entry<T> current = entry;
            while(current.next != previous) {
                current = current.next;
            }

            previous = current;
            newPrevious = current.setNext(newPrevious);
        }

        newList = new SimpleList<T>(newPrevious);
        return newList;
    }

    @Override
    public String toString() {
        Entry<T> current = entry;
        String message = "Values: ";
        while(current != null) {
            message += "\t" + current.value;
            current = current.next;
        }

        return message;
    }

    private class Entry<T> {
        public final T value;
        public final Entry next;

        public Entry() {
            value = null;
            next = null;
        }

        public Entry(T value) {
            this.value = value;
            next = null;
        }

        public Entry(T value, Entry<T> next) {
            this.value = value;
            this.next = next;
        }

        public Entry<T> setNext(Entry<T> next) {
            return new Entry(value, next);
        }

        public Entry<T> setValue(T value) {
            return new Entry(value, next);
        }
    }
}
