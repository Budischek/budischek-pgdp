package Blatt11;

/**
 * Created by david on 21.01.2017.
 */
public class NotEnoughSpecial extends NotEnoughExc{
    public NotEnoughSpecial(int should, int is) {
        super(should, is);
    }

    @Override
    public String toString() {
        return "Password doesn't contain enough special characters. You entered " + is + " special characters. Please enter " + should + " special characters";
    }
}