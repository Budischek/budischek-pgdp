package Blatt6;
/**
 * Created by david on 30.11.16.
 */
public class Toolbox{
    public static int evenSum(int n) {
        int increment = 1;
        int sum = 0;
        if(n < 0) increment = -1;
        if(modulo(n, 2) == 1) n -= increment;
        increment += increment;
        if(n == 0) return 0;
        else return evenSum(n - increment) + n;
    }

    public static int multiplication(int x, int y) {
        if(y == 0) return 0;
        else if(y == 1) return x;
        else if(y == -1) return -x;
        else if (y > 0) return multiplication(x, y - 1) + x;
        else return multiplication(x, y + 1) - x;
    }

    public static void reverse(int[] m) {
        reverse(m, 0);
    }

    public static int numberOfOddIntegers(int[] m) {
        return numberOfOddIntegers(m, 0);
    }

    public static int[] filterOdd(int[] m) {
        int[] result =  new int[m.length- countEven(m, 0)];
        return removeEven(m, result, 0, 0);
    }

    public static int numberOfOddIntegers(int[] m, int position) {
        if(position >= m.length) return 0;
        if(modulo(m[position], 2) == 1) return numberOfOddIntegers(m, position + 1) + 1;
        return numberOfOddIntegers(m, position + 1) ;
    }

    public static int[] reverse(int[] m, int count) {
        if(count + count >= m.length-1) return m;
        int tmp = m[count];
        m[count] = m[m.length - 1 - count];
        m[m.length - 1 - count] = tmp;
        return reverse(m, count+1);
    }

    public static int countEven(int[] m, int position) {
        if(position >= m.length) return 0;
        if(modulo(m[position], 2) == 0) return countEven(m, position +1) + 1;
        return countEven(m, position + 1);
    }

    public static int[] removeEven(int[] m, int[] result, int positionOld, int positionNew) {
        if(positionOld >= m.length || positionNew >= result.length) return result;
        if(modulo(m[positionOld], 2) == 0) return removeEven(m, result, positionOld + 1, positionNew);
        result[positionNew] = m[positionOld];
        return removeEven(m, result, positionOld + 1, positionNew + 1);
    }

    /**
     * A recursive implementation of the % Operator
     */
    public static int modulo(int dividend, int divisor) {
        if(dividend >= 0) {
            if (dividend < divisor) return dividend;
            else return modulo(dividend - divisor, divisor);
        }
        else return modulo(dividend + divisor, divisor);
    }

    public static void main(String[] args) {
        System.out.println(modulo(0,2));
    }
}
