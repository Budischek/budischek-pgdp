package Blatt6;

/**
 * Determine the amount of multiplications required if multiplying in the most effective order.
 */
public class MatrixMultOptimization {

    public static int f(int[][] mm) {
        return f(mm, 0, mm.length - 1);
    }

    public static int f(int[][] mm, int i, int j) {
        if(i == j || i > j) return 0;
        int tmp = 0;
        int min = Integer.MAX_VALUE;
        for(int count = i; count < j; count++) {
            tmp = f(mm, i, count) + f(mm, count + 1, j) + (mm[i][0] * mm[count][1] * mm[j][1]);
            if(tmp < min) min = tmp;
        }
        return min;
    }

}
