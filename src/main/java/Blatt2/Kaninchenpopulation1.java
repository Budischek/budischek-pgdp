package Blatt2;
import MiniJava.MiniJava;

/**
 * Created by david on 02.11.16.
 */
public class Kaninchenpopulation1 extends MiniJava{
    public static void main(String[] args) {
        int n = read();
        if (n<1) {
            write("Keine gültige Eingabe");
            System.exit(1);
        }
        int gen1 = 1, gen2 = 0, gen3 = 0;
        while (n>1) {
            int tmp = gen1 + gen2 + gen3;
            gen3 = gen2;
            gen2 = gen1;
            gen1 = tmp;
            n--;
        }
        String out = "1. Generation: " + gen1 + " --- 2. Generation: " + gen2 + " --- 3. Generation: " + gen3;
        write(out);
    }
}