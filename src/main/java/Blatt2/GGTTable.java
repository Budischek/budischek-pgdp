package Blatt2;
import MiniJava.MiniJava;
/**
 * Created by david on 02.11.16.
 */
public class GGTTable extends MiniJava{
    public static void main(String[] args) {
        int n = read();
        if (n<1) {
            write("Keine gültige Eingabe");
            System.exit(1);
        }
        String out = "";
        for (int i = 0; i <= n; i++) {
            out = out + i + "\t";
        }
        out = out + "\n";
        for(int i=1; i <= n; i++) {
            out = out + i;
            for(int j=1; j <= n; j++) {
                int a = i;
                int b = j;
                while(a!=b) {
                    if (b > a) {
                        b = b-a;
                    }
                    else {
                        a = a-b;
                    }
                }
                out = out + "\t" + a;
            }
            out = out + "\n";
        }
        System.out.println(out);
    }
}
