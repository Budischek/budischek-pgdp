package Blatt12;

/**
 * Created by david on 25.01.17.
 */
public class RockPaperScissors implements Runnable {
    public void run() {
        Player p1 = new Player();
        Player p2 = new Player();
        Thread t1 = new Thread(p1);
        Thread t2 = new Thread(p2);
        t1.start();
        t2.start();
        int p1win = 0, p2win = 0, draw = 0;
        int p1Choice, p2Choice;
        for(int i = 0; i < 1000; i++) {
            try {
                p1Choice = p1.getChoice();
                p2Choice = p2.getChoice();
                if(p1Choice == p2Choice) draw++;
                else if(p1Choice == p2Choice + 1 || (p1Choice == 0 && p2Choice == 2)) p1win++;
                else p2win++;
            }
            catch (InterruptedException e) {
                System.out.println("Interrupt");
            }
        }

        t1.interrupt();
        t2.interrupt();

        System.out.println("Thread 1 won " + p1win + " times. Thread 2 won " + p2win + " times. They drew " + draw + " timess.");
    }

    public static void main(String[] args) {
        RockPaperScissors rps = new RockPaperScissors();
        Thread t = new Thread(rps);
        t.start();
    }
}
