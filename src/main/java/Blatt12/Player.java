package Blatt12;
import java.util.Random;
/**
 * Created by david on 25.01.17.
 */
public class Player implements Runnable{
    int choice = 0;

    public synchronized void run() {
        Random rand = new Random();
        while(choice != -2) {
            try {
                while (choice > -1) {
                    wait();
                }
                choice = rand.nextInt(2);
                notify();
            }
            catch(InterruptedException e){
                choice = -2;
            }
        }
    }

    public synchronized int getChoice() throws InterruptedException{
        while(choice < 0) {
            wait();
        }
        int c = choice;
        choice = -1;
        notify();
        return c;
    }

}
