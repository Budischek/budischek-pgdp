package Blatt12;

/**
 * Created by david on 26.01.2017.
 */
public class Map {
    public static <T, R> void map(Fun<T, R> f, T[] a, R[]b, int n) throws InterruptedException {
        String excMsg = "";
        if(f == null || a == null || b == null) {
            excMsg = "Null is not a valid argument";
        }
        else if(a.length > b.length) {
            excMsg = "a too long to map into b";
        }
        else if(n > a.length) {
            excMsg = "Cannot create more Threads than there are values in a";
        }

        if(excMsg != "") {
            throw new IllegalArgumentException(excMsg);
        }
        MapThread[] threads = new MapThread[n];
        int threadSize = a.length/n;
        int thisSize;
        int position = 0;
        //You cannot instantiate a non static inner class within a static method without first instantiating the outer class
        Map map = new Map();
        for(int i = 0; i < n; i++) {
            if(i < a.length%n) thisSize = threadSize + 1;
            else thisSize = threadSize;
            threads[i] = map.new MapThread<T, R>(f, a, b, position, position + thisSize);
            position += thisSize;
        }
        try{
            boolean done = false;
            while(!done) {
                done = true;
                for(MapThread mt : threads) {
                    if(!mt.get()) {
                        done = false;
                        break;
                    }
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Integer[] a = new Integer[]{1, 2, 3, 4, 5, 6};
        String[] b = new String[9];
        try {
            Map.map(new IntToString(), a, b, 4);
        }
        catch(Exception e) {
            System.out.println(e);
        }

        for(int i = 0; i < a.length; i++) {
            System.out.println(a[i] + "->" + b[i]);
        }
    }

    private class MapThread<T, R> implements Runnable {
        private T[] a;
        private R[] b;
        private int from, to;
        private Fun<T, R> fun;
        private Thread thread;
        private boolean finished = false;

        public MapThread(Fun<T, R> f, T[] a, R[] b, int from , int to) {
            this.a = a;
            this.b = b;
            this.from = from;
            this.to = to;
            this.fun = f;
            this.thread = new Thread(this);
            thread.start();
        }

        public void run() {
            for(int i = from; i < to; i++) {
                b[i] = fun.apply(a[i]);
            }
            finished = true;
        }

        public boolean get() throws InterruptedException{
            try {
                thread.join();
                return finished;
            }
            catch(InterruptedException e) {
                return false;
            }
        }
    }
}
