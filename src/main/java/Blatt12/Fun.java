package Blatt12;

/**
 * Created by david on 26.01.2017.
 */
public interface Fun<T, R> {
    public R apply(T x);
}
