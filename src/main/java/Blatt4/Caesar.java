package Blatt4;

import MiniJava.MiniJava;

/**
 * Created by David on 16.11.2016.
 * Encrypt a string using the Caesar Cipher
 * Case sensitive
 * Only cipher the alphabet, ignore other symbols/numbers
 */
public class Caesar extends MiniJava{

    private static String lowerAlphabet = "abcdefghijklmnopqrstuvwxyz";
    private static String upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static void main(String[] args) {
        String input = readString("Please input your text");
        int shift = readInt("Please input your shift value");
        String output;

        output = cipher(input, shift);
        write(output);

    }

    public static String cipher(String input, int shift) {
        String output = "";
        for(int i=0; i<input.length(); i++) {
            output += parseChar(input.charAt(i), shift);
        }
        return output;
    }


    public static char parseChar(char c, int shift) {
        int tmp = 0;
        for(int i=0; i<lowerAlphabet.length(); i++) {
            if (c == lowerAlphabet.charAt(i)) {
                tmp = i+shift;
                while(tmp < 0) tmp += lowerAlphabet.length();
                tmp %= lowerAlphabet.length();
                return lowerAlphabet.charAt(tmp);
            }
            if (c == upperAlphabet.charAt(i)) {
                tmp = i+shift;
                while(tmp < 0) tmp += upperAlphabet.length();
                tmp %= upperAlphabet.length();
                return upperAlphabet.charAt(tmp);
            }
        }
        return c;
    }
}
