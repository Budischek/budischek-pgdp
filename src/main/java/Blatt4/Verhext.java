package Blatt4;
import MiniJava.MiniJava;
/**
 * Created by david on 11.11.16.
 * Read a hexadecimal number as String
 * Output the number as integer
 * Assumptions:
 *      Input is a valid hexadecimal number
 *      Input number will always fit into an integer
 *
 * Development:
 *      Step 1: basic conversion (e.g. "0x5Ab", "0Xabc", "0X1A3")
 *      Step 2: Allow underscores as part of the number (e.g. "0xA_B_C", "0X98_F_3")
 *      Step 3: Allow negativ Numbers (e.g. "-0xFfF")
 *      BONUS: Step 4: Validate inputs (e.g. "0xX", "--0xABC")
 *
 * Requirements:
 *      Only use the following Methods:
 *          length()
 *          charAt(int index)
 *          pow(int x, int y)
 *          MiniJava-Methods
 */
public class Verhext extends MiniJava {

// x^y

    public static int pow(int x, int y) {

        return java.math.BigInteger.valueOf(x).pow(y).intValueExact();

    }

    public static void main(String[] args) {

        String input = readString();

        int output;

        output = hexToInt(input);
        write(output);

    }

    public static int hexToInt(String hex) {
        int integerValue = 0;
        String number = "";
        int charValue;
        Boolean negative = false;
        int i = 0;
//      Prevent Index out of bound exceptions
        if(hex.length()<3) {
            error("Input too short (Prefix is required)");
        }
//      Check for negative numbers
        if(hex.charAt(0) == '-') {
            negative = true;
            i++;
        }
//      Check Prefix
        if(hex.charAt(i) == '0' && (hex.charAt(i+1) == 'x' || hex.charAt(i+1) == 'X')) {
            i+=2;
        }
        else {
            error("Invalid prefix");
        }

//      This is in accordance with the Java Documentation
        if(hex.charAt(i) == '_' || hex.charAt(hex.length()-1) == '_') {
            error("Number cannot start or end with an underscore");
        }
//      Remove all legal Underscores
        while(i<hex.length()) {
            if(hex.charAt(i)!= '_') {
                number += hex.charAt(i);
            }
            i++;
        }
//      Parse each character into their corresponding integer value
        for(i=0; i<number.length(); i++) {
            charValue = parseChar(number.charAt(i));
            if(charValue == 99) {
                error("Invalid character in Number");
            }
            else integerValue += charValue * pow(16, number.length()-(i+1));
        }
        if(negative) {
            integerValue *= -1;
        }
        return integerValue;
    }


    public static int parseChar(char c) {
        String lowerHex = "0123456789abcdef";
        String upperHex = "0123456789ABCDEF";

        for(int i=0; i<lowerHex.length(); i++) {
            if(c == lowerHex.charAt(i) || c == upperHex.charAt(i)) {
                return i;
            }
        }

        return 99;
    }

    public static void error(String msg) {
        write(msg + "\nThe program will now exit");
        System.exit(1);
    }
}