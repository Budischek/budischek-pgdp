package Blatt4;

/**
 * Created by David on 19.11.2016.
 */
public class Seiteneffekte {
    public static void main(String[] args) {
        System.out.println("Aufgabe 1:");
        aufgabe1();
        System.out.println("Aufgabe 2:");
        aufgabe2();
        System.out.println("Aufgabe 3:");
        aufgabe3();
    }

    public static void aufgabe1() {
        int a1 = 0;
        int b1 = 10;
        int a2 = 0;
        int b2 = 10;

        a1 = b1++ + ++b1 - --b1;

        a2 = b2 + 1;
        b2 += 1;

        System.out.println("A1 " + a1 + "||A2 " + a2 + "||B1 " + b1 + "||B2 " + b2);
    }

    public static void aufgabe2() {
        int a1 = 0;
        int b1 = 10;
        int a2 = 0;
        int b2 = 10;

        a1 = b1 + 1 - --b1;

        a2 = 2;
        b2 -= 1;

        System.out.println("A1 " + a1 + "||A2 " + a2 + "||B1 " + b1 + "||B2 " + b2);
    }

    public static void aufgabe3() {
        int a1 = 0;
        int b1 = 10;
        int a2 = 0;
        int b2 = 10;

        a1 = b1++ + b1++ + b1++;

        a2 = 3*b2 + 3;
        b2 += 3;

        System.out.println("A1 " + a1 + "||A2 " + a2 + "||B1 " + b1 + "||B2 " + b2);
    }
}
