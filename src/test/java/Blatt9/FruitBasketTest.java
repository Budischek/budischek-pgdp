package Blatt9;

import Blatt9.pgdp.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by David on 16.12.2016.
 */
public class FruitBasketTest {
    FruitBasket tester;

    @Before
    public void init() {
        tester = new FruitBasket();
    }

    @Test
    public void getApples_3differentAppleTypes_returnAll3() {
        tester.addFruit(new GrannySmith());
        tester.addFruit(new Apple());
        tester.addFruit(new PinkLady());
        Assert.assertEquals(3, tester.getApples().size());
    }

    @Test
    public void getApples_3differentFruitsNoApples_returnNone() {
        tester.addFruit(new Pineapple());
        tester.addFruit(new Fruit());
        tester.addFruit(new Banana());
        Assert.assertEquals(0, tester.getApples().size());
    }

    @Test
    public void getShelfLife_2Lower1Higher_return1() {
        tester.addFruit(new Pineapple());
        tester.addFruit(new Apple());
        tester.addFruit(new Banana());
        Assert.assertEquals(1, tester.getEqualOrLongerShelfLife(21).size());
    }
}
