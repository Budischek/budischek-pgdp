package Blatt9;

import Blatt9.pgdp.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by David on 16.12.2016.
 */
public class FruitTest {
    Fruit tester;

    @Test
    public void apple_isApple_true() {
        tester = new Apple();
        Assert.assertTrue(tester.isApple());
        Assert.assertFalse(tester.isBanana());
        Assert.assertFalse(tester.isPineapple());
        Assert.assertEquals(30, tester.shelfLife());
    }

    @Test
    public void banana_isBanana_true() {
        tester = new Banana();
        Assert.assertTrue(tester.isBanana());
        Assert.assertFalse(tester.isApple());
        Assert.assertFalse(tester.isPineapple());
        Assert.assertEquals(7, tester.shelfLife());
    }

    @Test
    public void pineapple_isPineapple_true() {
        tester = new Pineapple();
        Assert.assertTrue(tester.isPineapple());
        Assert.assertFalse(tester.isBanana());
        Assert.assertFalse(tester.isApple());
        Assert.assertEquals(20, tester.shelfLife());
    }

    @Test
    public void grannySmith_isApple_true() {
        tester = new GrannySmith();
        Assert.assertTrue(tester.isApple());
        Assert.assertFalse(tester.isBanana());
        Assert.assertFalse(tester.isPineapple());
        Assert.assertEquals(50, tester.shelfLife());
    }

    @Test
    public void pinkLady_isApple_true() {
        tester = new PinkLady();
        Assert.assertTrue(tester.isApple());
        Assert.assertFalse(tester.isBanana());
        Assert.assertFalse(tester.isPineapple());
        Assert.assertEquals(20, tester.shelfLife());
    }
}
