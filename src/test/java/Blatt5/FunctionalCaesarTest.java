package Blatt5;

import Blatt4.Caesar;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by david on 14.11.16.
 *  *      Step 2: Allow underscores as part of the number (e.g. "0xA_B_C", "0X98_F_3")
 *      Step 3: Allow negativ Numbers (e.g. "-0xFfF")
 *      BONUS: Step 4: Validate inputs (e.g. "0xX", "--0xABC")
 */
public class FunctionalCaesarTest {
    @Test
    public void testCipher() {
        FunctionalCaesar tester = new FunctionalCaesar();

        assertEquals("Khoor Vwxghqwv! .dDeEfF? >zZaAbBcC<", tester.encrypt("Hello Students! .aAbBcC? >wWxXyYzZ<", 3));
        assertEquals("Hello Students! .aAbBcC? >wWxXyYzZ<", tester.encrypt(tester.encrypt("Hello Students! .aAbBcC? >wWxXyYzZ<", 3), -3));
        assertEquals("ABC", tester.encrypt("XYZ",29));
        assertEquals("UVW", tester.encrypt("XYZ",-29));
        assertEquals("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG", tester.decrypt("QEB NRFZH YOLTK CLU GRJMP LSBO QEB IXWV ALD", -3));
    }
}