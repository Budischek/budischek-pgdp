package Blatt8;

import Blatt8.SymmetricStack;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by David on 10.12.2016.
 */
public class SymmetricStackTest {
    private SymmetricStack stack;

    @Before
    public void setup() {
        stack = new SymmetricStack();
    }

    //region ConstructorTests
    @Test
    public void constructor_noParameter_10elementArray() {
        assertStack("* * * * * * * * * * ");
    }
    //endregion

    //region numberOfElementsTests
    @Test
    public void numberOfElements_emptyList_returns0() {
        Assert.assertEquals(0, stack.getNumberOfElements());
    }

    @Test
    public void numberOfElements_append3Elements_returns3() {
        batchAppend(1,2,3);
        Assert.assertEquals(3, stack.getNumberOfElements());
    }

    @Test
    public void numberOfElements_prepend3Elements_returns3() {
        batchPrepend(1,2,3);
        Assert.assertEquals(3, stack.getNumberOfElements());
    }

    @Test
    public void numberOfElements_append6Elements_returns6() {
        batchAppend(1,2,3,4,5,6);
        Assert.assertEquals(6, stack.getNumberOfElements());
    }

    @Test
    public void numberOfElements_prepend7Elements_returns7() {
        batchPrepend(1,2,3,4,5,6,7);
        Assert.assertEquals(7, stack.getNumberOfElements());
    }
    //endregion

    //region isEmptyTests
    @Test
    public void isEmpty_emptyList_returnsTrue() {
        Assert.assertTrue(stack.isEmpty());
    }

    @Test
    public void isEmpty_nonEmptyList_returnsFalse() {
        stack.append(1);
        Assert.assertFalse(stack.isEmpty());
    }
    //endregion

    //region isFulLTests
    @Test
    public void isFull_emptyList_returnsFalse() {
        Assert.assertFalse(stack.isFull());
    }

    @Test
    public void isFull_nonEmptyList_returnsFalse() {
        stack.append(1);
        Assert.assertFalse(stack.isEmpty());
    }

    @Test
    public void isFull_wrappedList_returnsFalse() {
        batchAppend(1,2,3,4,5,6);
        Assert.assertFalse(stack.isEmpty());
    }

    @Test
    public void isFull_fullList_returnsTrue() {
        batchAppend(1,2,3,4,5,6,7,8,9,10);
        Assert.assertFalse(stack.isEmpty());
    }
    //endregion

    //region increaseTests
    @Test
    public void increase_add11Elements_doubleSizeOfStack() {
        batchAppend(1,2,3,4,5,6,7,8,9,10,11);
        assertStack("* * * * * (1 2 3 4 5 6 7 8 9 10 11)* * * * ");
    }
    //endregion

    //region decreaseTests
    @Test
    public void decrease_addAndRemoveElements_HalfSizeOfStack() {
        batchAppend(1,2,3);
        stack.removeFirst();
        assertStack("* (2 3)* * ");
    }
    //endregion

    //region appendTests
    @Test
    public void append_addToEmptyList_elementInTheMiddle() {
        stack.append(1);
        assertStack("* * * * * (1)* * * * ");
    }

    @Test
    public void append_add6Elements_wrapAround() {
        batchAppend(1,2,3,4,5,6);
        assertStack(" 6)* * * * (1 2 3 4 5");
    }
    //endregion

    //region prependTests
    @Test
    public void prepend_addToEmptyList_elementInTheMiddle() {
        stack.prepend(1);
        assertStack("* * * * * (1)* * * * ");
    }

    @Test
    public void prepend_add6Elements_wrapAround() {
        batchPrepend(1,2,3,4,5,6, 7);
        assertStack(" 6 5 4 3 2 1)* * * (7");
    }
    //endregion

    //region removeFirstTests
    @Test
    public void removeFirst_add7Remove1_6ElementStack() {
        batchAppend(1,2,3,4,5,6);
        stack.removeFirst();
        assertStack(" 6)* * * * * (2 3 4 5");
    }

    @Test
    public void removeFirst_wrapAround_wrapBack() {
        batchAppend(1,2,3,4,5,6,7,8,9);
        batchRemoveFirst(5);
        assertStack("(6 7 8 9)* * * * * * ");
    }

    @Test
    public void removeFirst_emptyStack_HalfStacksize(){
        stack.removeFirst();
        assertStack("* * * * * ");
    }
    //endregion

    //region removeLastTests
    @Test
    public void removeLast_add5Remove1_4ElementStack() {
        batchAppend(1,2,3,4,5);
        stack.removeLast();
        assertStack("* * * * * (1 2 3 4)* ");
    }

    @Test
    public void removeLast_wrapAround_wrapBack() {
        batchAppend(1,2,3,4,5,6,7,8,9);
        batchRemoveLast(3);
        assertStack("* * * * * (1 2 3 4 5)");
    }

    @Test
    public void removeLast_emptyStack_HalfStacksize(){
        stack.removeLast();
        assertStack("* * * * * ");
    }
    //endregion

    private void assertStack(String print) {
        Assert.assertEquals(print, stack.toString());
    }

    private void batchAppend(int... values) {
        for (int i : values) stack.append(i);
    }

    private void batchPrepend(int... values) {
        for (int i : values) stack.prepend(i);
    }

    private void batchRemoveFirst(int count) {
        for (int i = 0; i < count; i++) stack.removeFirst();
    }

    private void batchRemoveLast(int count) {
        for (int i = 0; i < count; i++) stack.removeLast();
    }
}
