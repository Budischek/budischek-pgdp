package Blatt8.pgdp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by David on 13.12.2016.
 */
public class BankAccountTest {
    public BankAccount tester;

    @Before
    public void setup() {
        tester = new BankAccount(0, "fname", "sname");
    }

    //region Constructor
    @Test
    public void constructor_basicValues_returnBankAccount() {
        tester = new BankAccount(1, "Firstname", "surname");
        Assert.assertEquals(1, tester.getAccountnumber());
        Assert.assertEquals("Firstname", tester.getFirstname());
        Assert.assertEquals("surname", tester.getSurname());
    }
    //endregion

    @Test
    public void addMoney_add100ToZero_Money100Cent() {
        tester.addMoney(new Money(100));
        Assert.assertEquals(100, tester.getBalance().getCent());
    }

    @Test
    public void toString_basicBankAccount_correctString() {

        Assert.assertEquals("Account: 0 Surame: sname Firstname: fname Balance: 0,00 Euro", tester.toString());
    }
}
