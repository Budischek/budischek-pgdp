package Blatt8.pgdp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by David on 13.12.2016.
 */
public class BankTest {
    public Bank tester;

    @Before
    public void setup() {
        tester = new Bank();
    }

    @Test
    public void newAccount_create5Accounts_printAccounts() {
        for(int i = 0; i < 5; i++) {
            tester.newAccount("" + i, "" + (i + 5));
        }
        Assert.assertEquals("0,00 Euro\t 0,00 Euro\t 0,00 Euro\t 0,00 Euro\t null\t null\t ", printBank(1 , 2, 3, 4, 5, 6));
    }

    @Test
    public void removeAccount_create5AccountsRemove2_printAccounts() {
        for(int i = 0; i < 5; i++) {
            tester.newAccount("" + i, "" + (i + 5));
        }
        tester.removeAccount(2);
        tester.removeAccount(4);
        Assert.assertEquals("0,00 Euro\t null\t 0,00 Euro\t null\t null\t null\t ", printBank(1 , 2, 3, 4, 5, 6));
    }

    @Test
    public void depositOrWithdraw_create5AccountsDepositeTo2_printAccounts() {
        for(int i = 0; i < 5; i++) {
            tester.newAccount("" + i, "" + (i + 5));
        }
        Assert.assertTrue(tester.depositOrWithdraw(2, new Money(10010)));
        Assert.assertTrue(tester.depositOrWithdraw(3, new Money(-100)));
        Assert.assertFalse(tester.depositOrWithdraw(-8, new Money(1000)));
        Assert.assertEquals("0,00 Euro\t 100,10 Euro\t -1,00 Euro\t 0,00 Euro\t null\t null\t ", printBank(1 , 2, 3, 4, 5, 6));
    }

    @Test
    public void transfer_create5AccountsTransferSuccesfully_printAccounts() {
        for(int i = 0; i < 5; i++) {
            tester.newAccount("" + i, "" + (i + 5));
        }
        Assert.assertTrue(tester.transfer(2, 3, new Money(10010)));
        Assert.assertEquals("0,00 Euro\t -100,10 Euro\t 100,10 Euro\t 0,00 Euro\t null\t null\t ", printBank(1 , 2, 3, 4, 5, 6));
    }

    @Test
    public void transfer_create5AccountsTransferUnsuccesfully_NoChanges() {
        for(int i = 0; i < 5; i++) {
            tester.newAccount("" + i, "" + (i + 5));
        }
        Assert.assertFalse(tester.transfer(2, 7, new Money(10010)));
        Assert.assertEquals("0,00 Euro\t 0,00 Euro\t 0,00 Euro\t 0,00 Euro\t null\t null\t ", printBank(1 , 2, 3, 4, 5, 6));
    }

    private String printBank(int... accounts) {
        String output = "";
        for(int i : accounts) {
            output += tester.getBalance(i) + "\t ";
        }
        return output;
    }
}
