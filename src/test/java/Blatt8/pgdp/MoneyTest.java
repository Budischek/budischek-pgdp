package Blatt8.pgdp;

import Blatt8.pgdp.Money;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by David on 13.12.2016.
 */
public class MoneyTest {
    public Money tester;

    @Before
    public void setup() {
        tester = new Money();
    }

    //region Constructor
    @Test
    public void constructor_noParam_0cent() {
        tester = new Money();
        Assert.assertEquals(0, tester.getCent());
    }

    @Test
    public void constructor_randomParam_returnCent() {
        int input;
        for(int i = 0; i < 100; i++) {
            input = ((int) Math.random() * 200) - 100;
            tester = new Money(input);

            Assert.assertEquals(input, tester.getCent());;
        }
    }
    //endregion

    @Test
    public void addMoney_randomParam_returnCent() {
        int input;
        int sum = 0;
        for(int i = 0; i < 100; i++) {
            input = ((int) Math.random() * 1000) - 500;
            sum += input;
            tester = tester.addMoney(new Money(input));

            Assert.assertEquals(sum, tester.getCent());;
        }
    }

    @Test
    public void toString_emptyParam_string() {
        Assert.assertEquals("0,00 Euro", tester.toString());
    }

    @Test
    public void toString_10010cent_string() {
        tester = tester.addMoney(new Money(10010));

        Assert.assertEquals("100,10 Euro", tester.toString());
    }

    @Test
    public void toString_negativeCent_signedAmount() {
        tester = tester.addMoney(new Money(-1010));

        Assert.assertEquals("-10,10 Euro", tester.toString());
    }

    @Test
    public void toString_zeroCent_0centString() {
        tester = tester.addMoney(new Money(0));

        Assert.assertEquals("0,00 Euro", tester.toString());
    }

    @Test
    public void immutable_addMoney() {
        Money before = tester;
        tester.addMoney(new Money(5000));

        Assert.assertEquals(before, tester);
    }

    @Test
    public void immutable_getCent() {
        Money before = tester;
        tester.toString();

        Assert.assertEquals(before, tester);
    }
}
