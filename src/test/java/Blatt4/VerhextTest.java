package Blatt4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import static org.junit.Assert.*;

/**
 * Created by david on 14.11.16.
 *  *      Step 2: Allow underscores as part of the number (e.g. "0xA_B_C", "0X98_F_3")
 *      Step 3: Allow negativ Numbers (e.g. "-0xFfF")
 *      BONUS: Step 4: Validate inputs (e.g. "0xX", "--0xABC")
 */
public class VerhextTest {

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Test
    public void testSimpleHexValues() {
        Verhext tester = new Verhext();

        assertEquals(1451, tester.hexToInt("0x5Ab"));
        assertEquals(2748, tester.hexToInt("0Xabc"));
        assertEquals(419, tester.hexToInt("0X1A3"));
    }

    @Test
    public void testUnderscores() {
        Verhext tester = new Verhext();

        assertEquals(2748, tester.hexToInt("0xA_B_C"));
        assertEquals(39155, tester.hexToInt("0X98_F_3"));
    }

    @Test
    public void testNegativeNumbers() {
        Verhext tester = new Verhext();

        assertEquals(-4095, tester.hexToInt("-0xFfF"));
    }

    @Test
    public void testValidateInputNaN() {
        Verhext tester = new Verhext();
        exit.expectSystemExit();
        tester.hexToInt("0xX");
    }

    @Test
    public void testValidateInputDoubleNegative() {
        Verhext tester = new Verhext();
        exit.expectSystemExit();
        tester.hexToInt("--0xABC");
    }

    @Test
    public void testValidateInputInvalidPrefix() {
        Verhext tester = new Verhext();
        exit.expectSystemExit();
        tester.hexToInt("0CABC");
    }

    @Test
    public void testValidateInputInvalidUnderscore() {
        Verhext tester = new Verhext();
        exit.expectSystemExit();
        tester.hexToInt("0xAB_");
    }



    @Test
    public void testParseChar() {
        Verhext tester = new Verhext();

        assertEquals(tester.parseChar('a'), 10);
        assertEquals(tester.parseChar('B'), 11);
        assertEquals(tester.parseChar('C'), 12);
        assertEquals(tester.parseChar('D'), 13);
        assertEquals(tester.parseChar('e'), 14);
        assertEquals(tester.parseChar('F'), 15);
        assertEquals(tester.parseChar('G'), 99);
        assertEquals(tester.parseChar('1'), 1);
        assertEquals(tester.parseChar('2'), 2);
        assertEquals(tester.parseChar('3'), 3);
        assertEquals(tester.parseChar('7'), 7);
        assertEquals(tester.parseChar('9'), 9);
        assertEquals(tester.parseChar('X'), 99);
        assertEquals(tester.parseChar('Z'), 99);
    }

}