package Blatt4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import static org.junit.Assert.assertEquals;

/**
 * Created by david on 14.11.16.
 *  *      Step 2: Allow underscores as part of the number (e.g. "0xA_B_C", "0X98_F_3")
 *      Step 3: Allow negativ Numbers (e.g. "-0xFfF")
 *      BONUS: Step 4: Validate inputs (e.g. "0xX", "--0xABC")
 */
public class CaesarTest {
    @Test
    public void testCipher() {
        Caesar tester = new Caesar();

        assertEquals("Khoor Vwxghqwv! .dDeEfF? >zZaAbBcC<", tester.cipher("Hello Students! .aAbBcC? >wWxXyYzZ<", 3));
        assertEquals("Hello Students! .aAbBcC? >wWxXyYzZ<", tester.cipher(tester.cipher("Hello Students! .aAbBcC? >wWxXyYzZ<", 3), -3));
        assertEquals("ABC", tester.cipher("XYZ",29));
        assertEquals("UVW", tester.cipher("XYZ",-29));
        assertEquals("QEB NRFZH YOLTK CLU GRJMP LSBO QEB IXWV ALD", tester.cipher("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG", -3));
    }
}