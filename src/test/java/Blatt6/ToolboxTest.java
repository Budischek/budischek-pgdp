package Blatt6;

import Blatt6.Toolbox;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by david on 30.11.16.
 */
public class ToolboxTest {
    public static Toolbox tester;

    @Before
    public void setUp() throws Exception {
        tester = new Toolbox();
    }


    @Test
    public void testModulo() {
        int dividend, divisor;
        for(int i = 0; i < 20000; i++) {
            dividend = i - 10000;
            //in Java the result of a % operation is the remainder of a division, tester.modulo calculates the positive modolus
            assertEquals(Math.abs(dividend%2) , tester.modulo(dividend, 2));
        }
    }

    @Test
    public void testEvenSum() {
        Random rand = new Random();
        int tmp;
        for(int i = 0; i < 500; i++) {
            tmp = rand.nextInt(20000) - 10000;
            assertEquals(evenSum(tmp), tester.evenSum(tmp));
        }
        tmp = 1;
        assertEquals(evenSum(tmp), tester.evenSum(tmp));
        tmp = 10000;
        assertEquals(evenSum(tmp), tester.evenSum(tmp));
        tmp = -1;
        assertEquals(evenSum(tmp), tester.evenSum(tmp));
        tmp = -10000;
        assertEquals(evenSum(tmp), tester.evenSum(tmp));

    }

    @Test
    public void testNumOfOdd() {
        Random rand = new Random();
        int numOdd, numEven, countOdd, countEven;
        int[] input;
        for(int i = 0; i < 2000; i++) {
            numOdd = rand.nextInt(20);
            numEven = 20 - numOdd;
            countOdd = 0;
            countEven = 0;
            input = new int[numOdd + numEven];
            while(numOdd > countOdd && numEven > countEven) {
                if(numOdd > countOdd) {
                    input[countOdd + countEven] = -9999 + 2*(int)(rand.nextInt(10000));
                    countOdd++;
                }
                if(numEven > countOdd) {
                    input[countOdd+countEven] = (rand.nextInt(10000) - 5000) * 2;
                    countEven++;
                }
            }
            assertEquals(countOdd, tester.numberOfOddIntegers(input));
        }

    }

    public int evenSum(int max) {
        int sum = 0;
        if(max > 0) {
            for(int i=0;i<=max;i=i+2)
            {
                sum=sum+i;
            }
            return sum;
        }
        for(int i=0;i>=max;i=i-2)
        {
            sum=sum+i;
        }
        return sum;
    }



}